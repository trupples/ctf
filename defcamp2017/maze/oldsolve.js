"use strict";

const io = require('socket.io-client');


const rotR = [, 2, 3, 4, 1],
	rotL = [, 4, 1, 2, 3],
	dirS = [, "left", "top", "right", "bottom"];
let forward = 1, right = rotR[forward];

let last5 = [];

const FSM = {
	state: "start",
	transitions: {
		start: {
			ok: "stepright"
		},
		stepright: {
			ok: "rotateright",
			err: "stepfwd"
		},
		rotateright: {
			ok: "repeat"
		},
		stepfwd: {
			ok: "repeat",
			err: "rotateleft"
		},
		repeat: {
			ok: "stepright"
		},
		rotateleft: {
			ok: "repeat"
		}
	},
	functions: {
		start: ok,
		stepright: function() {
			//console.log("Stepping right");
			last5[0] = last5[1];
			last5[1] = last5[2];
			last5[2] = last5[3];
			last5[3] = last5[4];
			last5[4] = right;
			sock.emit("action", {type: "move", direction: right});
			/*if(laststatus.pos[dirS[right]]) {
				err();
			} else {
				ok();
			}*/
		},
		rotateright: function() {
			forward = right;
			right = rotR[forward];
			ok();
		},
		stepfwd: function() {
			//console.log("Stepping forward");
			last5[0] = last5[1];
			last5[1] = last5[2];
			last5[2] = last5[3];
			last5[3] = last5[4];
			last5[4] = forward;
			sock.emit("action", {type: "move", direction: forward});
		},
		repeat: ok,
		rotateleft: function() {
			right = forward;
			forward = rotL[right];
			ok();
		}
	}
};

function ok() {
	FSM.state = FSM.transitions[FSM.state].ok;
	FSM.functions[FSM.state]();
}

function err() {
	FSM.state = FSM.transitions[FSM.state].err;
	FSM.functions[FSM.state]();
}

let maze = [];
for(let i=0; i<20; i++) {
	maze[i] = [];
	for(let j=0; j<20; j++)
		maze[i][j] = {pos: {top: false, left: false, right: false, bottom: false}}
}

let laststatus = {pos: {top: true, left: true, right: true, bottom: true}};
let first = true;
function status(status) {
	if(first) {
		console.log("fs")
		first = false;
		ok();
		return;
	}
	let s = JSON.parse(status);
	if(s.level > laststatus.level && s.level > 0) {
		for(let i=0; i<20; i++) {
			maze[i] = [];
			for(let j=0; j<20; j++)
				maze[i][j] = {pos: {top: false, left: false, right: false, bottom: false}}
		}
	}
	if(s.level < laststatus.level && laststatus.level > 0) {
		console.log("shit")
	}
	laststatus = s;
	let x = laststatus.pos.x
	let y = laststatus.pos.y
	let l = laststatus.level
	maze[y][x] = s
	display(maze)
	console.log(x, y, l)
	ok()
}
function error_message(error_message) {
	err()
}
function message(message) {
	console.log("[MESSAGE] " + JSON.stringify(message));
}
function display(maze) {
	let o = ""
	for(let i=0; i<20; i++) {
		for(let j=0; j<20; j++) {
			o += "+"
			if(maze[i][j].pos.top) {
				o += "-";
			} else {
				o += " ";
			}
		}
		o += "\n"
		for(let j=0; j<20; j++) {
			if(maze[i][j].pos.left) {
				o += "|";
			} else {
				o += " ";
			}
			o += " ";
		}
		o += "\n"
	}
	o += "\n\n\n"
	console.log(o)
}
const sock = io.connect("ws://45.76.95.55:8031");
sock.on("status", status);
sock.on("error_message", error_message);
sock.on("message", message);