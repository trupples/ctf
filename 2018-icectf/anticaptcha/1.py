import requests
import re

agent = requests.session()

url = "https://s1bxz4pykgaekr8-anticaptcha.labs.icec.tf/"
text = agent.get(url).text
text = text.replace("\n", " ")
questions = re.findall(r"<tr>\s+<td>([^<]+)</td>", text)

print(text)

def handlePrime(matches):
	n = int(matches[0])
	d = 2
	while d*d <= n:
		if n%d==0:
			return "false"
		d += 1
	return "true"

def handleGCD(matches):
	a = int(matches[0])
	b = int(matches[1])
	while a != 0:
		r = b % a 
		b = a
		a = r
	return str(b)

def handleWords(matches):
	n = int(matches[0])
	words = matches[1].replace(".", "").split(" ")
	if n == 0:
		return ""
	else:
		return words[n-1]

capitals = {
	"Hawaii": "Honolulu",
	"Germany": "Berlin"
}

def handleCapital(matches):
	country = matches[0]
	if country in capitals:
		return capitals[country]
	else:
		raise "Capital for {} not known".format(country)

knownQuestionTypes = [
	(r"^Is (\d+) a prime number\?", handlePrime),
	(r"^What is the greatest common divisor of (\d+) and (\d+)\?", handleGCD),
	(r"^What is the (\d+)(?:th|nd|st|rd) word in the following line: (.+)", handleWords),
	(r"^What is the capital of (\w+)\?", handleCapital),
	(r"^What year is it\?", lambda x:"2018"),
	(r"^What color is the sky\?", lambda x:"blue"),
	(r"^Who directed the movie Jaws\?", lambda x:"Steven Spielberg"),
	(r"^What is the tallest mountain on Earth\?", lambda x:"Everest"),
	(r"^How many planets are between Earth and the Sun\?", lambda x:"2"),
	(r"^How many strings does a violin have\?", lambda x:"4"),
	(r"^Which planet is closest to the sun\?", lambda x:"Mercury")
]

answers = []

for question in questions:
	for regex, handler in knownQuestionTypes:
		result = re.match(regex, question, re.MULTILINE)
		if result:
			answers.append(handler(result.groups()))
			break
	else:
		print("Unknown question type: " + question)


print(agent.post(url, data={"answer": answers, "submit": "Sumbit Answers"}).text)
