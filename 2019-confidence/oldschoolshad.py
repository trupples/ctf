matr_map = """        4 {4pp   
       p {k4{ E  
      p 44p{ p   
       4 p       
        S        
                 
                 
                 
                 """

alphabet = "abcdefghijklmnopqrstuvwxyz012345678"
flag_alph = " p4{krule_ctf}"
dictionary = []

# 00 = scade linia, scade coloana ;
# 01 = scade linia, creste coloana;
# 10 = creste linia, scade coloana;
# 11 = creste linia, creste coloana


#"0f0000000000000000"

def print_flg(s):
    flg = "p4{dobry}"
    bin_str = bin(int("0ff000000000000000", 16))[2:]
    bin_str = bin_str.zfill(72)
    if(len(bin_str) % 2 != 0):
        bin_str = '0' + bin_str

    max_len = len(bin_str) // 2

    bucati_de_2 = [bin_str[j:j+2] for j in range(0, len(bin_str), 2)]

    print bin_str
    for i in range(9):
        # reverse bucati_de_2[i*4:i*4+4]
        print i
        tmp = bucati_de_2[i*4]
        bucati_de_2[i*4] = bucati_de_2[i*4+3]
        bucati_de_2[i*4+3] = tmp
        tmp = bucati_de_2[i*4+1]
        bucati_de_2[i*4+1] = bucati_de_2[i*4+2]
        bucati_de_2[i*4+2] = tmp

	tup_arr = bucati_de_2

    zero_list = [0] * 162 # 9 * 18
    lin = 4
    col = 8

    for tmp in tup_arr:
        tmp = tmp[::-1]
        if(tmp == '00'):
            if(lin > 0):
                lin -= 1

            if(col > 0):
                col -= 1
        elif(tmp == '01'):
            if(lin > 0):
                lin -= 1

            if(col < 17):
                col += 1
        elif(tmp == '10'):
            if(lin < 8):
                lin += 1

            if(col > 0):
                col -= 1
        elif(tmp == '11'):
            if(lin < 8):
                lin += 1

            if(col < 17):
                col += 1

        zero_list[lin * 18 + col] += 1
        if(zero_list[lin * 18 + col] >= len(flag_alph)):
            zero_list[lin * 18 + col] = len(flag_alph) - 1

    for i in range(len(zero_list)):
        zero_list[i] = flag_alph[zero_list[i]]

        if(i == 80):
            zero_list[80] = 'S'

    zero_list[lin * 18 + col] = 'E'

    for i in range(9):
        print(''.join(zero_list[i * 18:(i + 1) * 18]))


def test(s):
    bin_str = bin(int(str('p4{' + "abcdef" + '}')[::-1].encode('hex'), 16))[2:]
    if(len(bin_str) % 2 != 0):
        bin_str = '0' + bin_str

    max_len = len(bin_str) // 2

    tup_arr = [bin_str[2 * (j - 1):2 * j] for j in range(1, max_len + 1)]

    zero_list = [0] * 162 # 9 * 18

    startpos = 80
    endpos = 32

    lin = 4
    col = 8

    #print(len(matr_map))
    #print(matr_map[80])
    #print(matr_map[62])
    #print(matr_map[3 * 18 + 9])
    #print(matr_map[4 * 18 + 8])

    retrieved_buf = ""

    #print bin_str

    chspsd = 0
    for tmp in tup_arr:
        if(tmp == '00'):
            if(lin > 0):
                lin -= 1

            if(col > 0):
                col -= 1
        elif(tmp == '01'):
            if(lin > 0):
                lin -= 1

            if(col < 17):
                col += 1
        elif(tmp == '10'):
            if(lin < 8):
                lin += 1

            if(col > 0):
                col -= 1
        elif(tmp == '11'):
            if(lin < 8):
                lin += 1

            if(col < 17):
                col += 1

        chspsd += 1

        if(matr_map[lin * 18 + col] == ' '):
            print('woops a whitespace at ' + str(lin) + " : " + str(col))
            print('characters passed: ' + str(chspsd))
            return False

        zero_list[lin * 18 + col] += 1

        if(zero_list[lin * 18 + col] >= len(flag_alph)):
            print('woops longer than flag_alph')
            return False

    for i in zero_list:
        zero_list[i] = flag_alph[zero_list[i]]

    zero_list[startpos] = 'S'
    zero_list[lin * 18 + col] = 'E'
    new_str = ''.join(zero_list)

    print new_str
    print matr_map
    print "\n\n\n\n"

    if(new_str == matr_map):
        print("FLAG FOUND!")
        print(bin_str)
        print(s)
        return True

    return False




for i in alphabet:
    for j in alphabet:
        for k in alphabet:
            for l in alphabet:
                for m in alphabet:
                    #for o in alphabet:
                        #for p in alphabet:
                            #for q in alphabet:
                                #for r in alphabet:
                    print_flg(str(i + j + k + l + m))# == True):
                    exit(1)