map = """        4 {4pp   
       p {k4{ E  
      p 44p{ p   
       4 p       
        S        
                 
                 
                 
                 
"""

def string2bits(s=''):
	return [bin(ord(x))[2:].zfill(8) for x in s]

def bits2string(b=None):
	B = (b[i:i+8].zfill(8) for i in range(0, len(b), 8))
	return ''.join([chr(int(x, 2)) for x in B])

possibilities = []
for i in range(9*18):
	possibilities.append([])

	lin, col = divmod(i, 18)

	possibilities[i].append(max(lin-1, 0)*18 + max(col-1, 0))
	possibilities[i].append(max(lin-1, 0)*18 + min(col+1, 16))
	possibilities[i].append(min(lin+1, 8)*18 + max(col-1, 0))
	possibilities[i].append(min(lin+1, 8)*18 + min(col+1, 16))

print possibilities

alphabet = " p4{krule_ctf}SE\n"

startpos = map.index("S")
endpos = map.index("E")


# 00 = -19 = StangaSus
# 01 = -17 = DreaptaSus
# 10 = 17 = StangaJos
# 11 = 19 = DreaptaJos

capacitate = [None] * len(map)

for i in range(len(map)):
	lin = i // 18
	col = i % 18

	capacitate[i] = alphabet.index(map[i])
	if map[i] == '\n':
		capacitate[i] = 0

def bkt(pos, binstr, moves):
	#print pos, binstr
	if capacitate[pos] <= 0:
		return

	if moves > 35 and map[pos] == 'E':
		b = bin(binstr)[3:-1]
		bb = ''
		for i in range(9):
			for j in range(4):
				p = i*8 + 6 - 2*j
				twobits = b[p:p+2]
				bb += twobits
		#print b, bb
		s = bits2string(bb)
		if len(repr(s)) - 2 == len(s):
			try:
				for c in s:
					if c not in "abcdefghijklmnopqrstuvwxyz0123456789{}=":
						break
				else:
					raise Exception()
				# asd
			except:
				print s
				pass

	capacitate[pos] -= 1
	#if moves < 12:
	#	direction = [0, 0, 3, 1, 0, 1, 3, 0, 3, 2, 3, 1][moves]
	#	newpos = possibilities[pos][direction]
	#	bkt(newpos, binstr*4 + direction, moves+1)
	#	return

	for direction in range(4):
		newpos = possibilities[pos][direction]
		bkt(newpos, binstr*4 + direction, moves+1)

	capacitate[pos] += 1

bkt(startpos, 1, 0)

# 01 11 00 00
# 00 11 01 00
# 01 11 10 11
#00 01 01 11