ct = open("dumbster.jpg", "rb").read()

for key in range(256):
	pt = ""

	for caracter_criptat in ct:
		caracter_decriptat = ord(caracter_criptat) ^ key
		pt += chr(caracter_decriptat)

	open("dumbster_decriptat_"+str(key)+".jpg", "wb").write(pt)
