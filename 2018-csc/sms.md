<<<<<<< HEAD
[Go back](.)

### SMS - 200 points

This task was riddled with misleading information from the organisers, all the
way from the vaguely written description to the wrong answer they gave to my
question regarding the y format ("de ce y1(0) este 72 adica H, cand de fapt este 4 adica 52?" = "why do you say y1(0) is 72 which is H when in fact it is 52 which is 4").

Initially I thought the y strings are hex encoded but after that response I
thought that maybe it's just a big coincidence that they only contain [0-9A-F]
characters and that I should treat them as length 30 ascii strings as opposed
to 15 bytes that are hex encoded. That didn't work, though, as the decrypted w
strings would contain invalid non-lowercase characters.

Quite a lot of time later I tried solving it like I initially set out to,
treating the given y strings as hex encoded bytes BUT allowing _any_ value to
make up x. This actually started leading to promising results.

First of all we need a list of all possible values for each position in x:

```py
import string
import sys

y = ["",
	"4840DDEB8C1CBAC721F824949FCF9A",
	"4840D5EA8E1BBACB36F72C9C94D59D",
	"484FD9EB9305A2C63FF92A909DD790",
	"4A4CD3F48018BFCC3BFC208598DE9A",
	"4A4CD0E28808B3C026F9289D98CF90",
	"4A4CCBEA9509A4CB2AF124819DDE9A",
	"4D4ACDE5911CB9C73CE4209F96D790"]

n = len(y[1])

valid_x_chars = "".join([chr(i) for i in range(256)])
valid_w_chars = string.lowercase

possible_x = [valid_x_chars for i in range(15)]

# For each ciphertext ct
for i in range(1, 8):
	ct = y[i].decode("hex")
	print ct

	# For each position in the ciphertext
	for j in range(15):
		bad_values = []
		# Test each possible x value
		for k in range(len(possible_x[j])):
			decrypted = chr(ord(possible_x[j][k]) ^ ord(ct[j]))
			if decrypted not in valid_w_chars:
				bad_values += [possible_x[j][k]]

		# And delete the ones that don't lead to valid decryptions
		for c in bad_values:
			possible_x[j] = possible_x[j].replace(c, "")

print("Possible characters for all positions of x:")
print(possible_x)
```

I then tried all the possible combinations of the first 3 characters of x:

```py
for x0 in possible_x[0]:
	for x1 in possible_x[1]:
		for x2 in possible_x[2]:
			X = x0+x1+x2
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(3)]) + " ")
```

There were lots of possible combinations but I managed to cut down that number
by quite a lot by removing the ones that resulted in non-english-like
decryptions (we know the decrypted w are english words).

First I removed all lines that had a word that begins with 3 consonants
( /[bcdfghjklmnpqrstvwxyz]{3}/ ) then I removed non-existent bigrams such as
'kk', 'kg', 'lk', 'kj', 'jx'. This left me with only 140 possible combinations.

The most english-like one was `2923be -> acc ack alg com con cou dis`. From now
on I assume x starts with `\x29\x23\xbe`.

Let's do the same thing but with the next 3 characters:

```py
for x3 in possible_x[3]:
	for x4 in possible_x[4]:
		for x5 in possible_x[5]:
			X = "\x29\x23\xbe"+x3+x4+x5
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(6)]) + " ")
```

Once again there are lots and lots of possible decryptions, but many wouldn't
make for great english words. A lot have four consonants in a row and these can
be removed, only leaving us with 16 (!!! yay) possible combinations:

```
2923be84e16a -> accomv acknoq algoro compar confib countc disapv 
2923be84e16b -> accomw acknop algorn compas confic countb disapw 
2923be84e16c -> accomp acknow algori compat confid counte disapp 
2923be84e16d -> accomq acknov algorh compau confie countd disapq 
2923be84e16e -> accomr acknou algork compav confif countg disapr 
2923be84e16f -> accoms acknot algorj compaw config countf disaps 
2923be84e170 -> accoml acknok algoru compah confix county disapl 
2923be84e171 -> accomm acknoj algort compai confiy countx disapm 
2923be84e17d -> accoma acknof algorx compae confiu countt disapa 
2923be84e17f -> accomc acknod algorz compag confiw countv disapc 
2923be84e76d -> accokq ackniv algoth compgu confoe counrd disavq 
2923be84e771 -> accokm acknij algott compgi confoy counrx disavm 
2923be84e77d -> accoka acknif algotx compge confou counrt disava 
2923be8ee17d -> accema ackdof algerx comzae conliu coudtt diskpa 
2923be8ee77d -> acceka ackdif algetx comzge conlou coudrt diskva 
2923be93fa7f -> accxvc ackytd algxiz comgzg conqrw couyov disvkc 
```

...the obvious winner being the third one. We can now see that the SMS words
are similar to `accomplish`, `acknowledge`, `algorithm`, `compatible`,
`confidential`, `disappointing` for x starting with `\x29\x23\xbe\x84\xe1\x6c`.

The next step is doing the same procedure knowing the first 6 characters of x:

```py
for x6 in possible_x[6]:
	for x7 in possible_x[7]:
		for x8 in possible_x[8]:
			X = "\x29\x23\xbe\x84\xe1\x6c"+x6+x7+x8
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(9)]) + " ")
```

Even though there still are lots of possible combinations we now know what to
look for and can search for the words we figured out earlier:

```
2923be84e16cd6ae52 -> accomplis acknowled algorithm compatibi confident counterex disappoin 
```

Doing this two more times gets us the decrypted sms messages and x:
```
2923be84e16cd6ae529049f1f1bbe9 -> accomplishments acknowledgement algorithmically compatibilities confidentiality counterexamples disappointingly 
```

The flag is x but with uppercase letters. It does conform with the "x is all
uppercase and digits" description but in a bad way :p

```
ECSC{2923BE84E16CD6AE529049F1F1BBE9}
```
=======
[Go back](.)

### SMS - 200 points

This task was riddled with misleading information from the organisers, all the
way from the vaguely written description to the wrong answer they gave to my
question regarding the y format ("de ce y1(0) este 72 adica H, cand de fapt este 4 adica 52?" = "why do you say y1(0) is 72 which is H when in fact it is 52 which is 4").

Initially I thought the y strings are hex encoded but after that response I
thought that maybe it's just a big coincidence that they only contain [0-9A-F]
characters and that I should treat them as length 30 ascii strings as opposed
to 15 bytes that are hex encoded. That didn't work, though, as the decrypted w
strings would contain invalid non-lowercase characters.

Quite a lot of time later I tried solving it like I initially set out to,
treating the given y strings as hex encoded bytes BUT allowing _any_ value to
make up x. This actually started leading to promising results.

First of all we need a list of all possible values for each position in x:

```py
import string
import sys

y = ["",
	"4840DDEB8C1CBAC721F824949FCF9A",
	"4840D5EA8E1BBACB36F72C9C94D59D",
	"484FD9EB9305A2C63FF92A909DD790",
	"4A4CD3F48018BFCC3BFC208598DE9A",
	"4A4CD0E28808B3C026F9289D98CF90",
	"4A4CCBEA9509A4CB2AF124819DDE9A",
	"4D4ACDE5911CB9C73CE4209F96D790"]

n = len(y[1])

valid_x_chars = "".join([chr(i) for i in range(256)])
valid_w_chars = string.lowercase

possible_x = [valid_x_chars for i in range(15)]

# For each ciphertext ct
for i in range(1, 8):
	ct = y[i].decode("hex")
	print ct

	# For each position in the ciphertext
	for j in range(15):
		bad_values = []
		# Test each possible x value
		for k in range(len(possible_x[j])):
			decrypted = chr(ord(possible_x[j][k]) ^ ord(ct[j]))
			if decrypted not in valid_w_chars:
				bad_values += [possible_x[j][k]]

		# And delete the ones that don't lead to valid decryptions
		for c in bad_values:
			possible_x[j] = possible_x[j].replace(c, "")

print("Possible characters for all positions of x:")
print(possible_x)
```

I then tried all the possible combinations of the first 3 characters of x:

```py
for x0 in possible_x[0]:
	for x1 in possible_x[1]:
		for x2 in possible_x[2]:
			X = x0+x1+x2
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(3)]) + " ")
```

There were lots of possible combinations but I managed to cut down that number
by quite a lot by removing the ones that resulted in non-english-like
decryptions (we know the decrypted w are english words).

First I removed all lines that had a word that begins with 3 consonants
( /[bcdfghjklmnpqrstvwxyz]{3}/ ) then I removed non-existent bigrams such as
'kk', 'kg', 'lk', 'kj', 'jx'. This left me with only 140 possible combinations.

The most english-like one was `2923be -> acc ack alg com con cou dis`. From now
on I assume x starts with `\x29\x23\xbe`.

Let's do the same thing but with the next 3 characters:

```py
for x3 in possible_x[3]:
	for x4 in possible_x[4]:
		for x5 in possible_x[5]:
			X = "\x29\x23\xbe"+x3+x4+x5
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(6)]) + " ")
```

Once again there are lots and lots of possible decryptions, but many wouldn't
make for great english words. A lot have four consonants in a row and these can
be removed, only leaving us with 16 (!!! yay) possible combinations:

```
2923be84e16a -> accomv acknoq algoro compar confib countc disapv 
2923be84e16b -> accomw acknop algorn compas confic countb disapw 
2923be84e16c -> accomp acknow algori compat confid counte disapp 
2923be84e16d -> accomq acknov algorh compau confie countd disapq 
2923be84e16e -> accomr acknou algork compav confif countg disapr 
2923be84e16f -> accoms acknot algorj compaw config countf disaps 
2923be84e170 -> accoml acknok algoru compah confix county disapl 
2923be84e171 -> accomm acknoj algort compai confiy countx disapm 
2923be84e17d -> accoma acknof algorx compae confiu countt disapa 
2923be84e17f -> accomc acknod algorz compag confiw countv disapc 
2923be84e76d -> accokq ackniv algoth compgu confoe counrd disavq 
2923be84e771 -> accokm acknij algott compgi confoy counrx disavm 
2923be84e77d -> accoka acknif algotx compge confou counrt disava 
2923be8ee17d -> accema ackdof algerx comzae conliu coudtt diskpa 
2923be8ee77d -> acceka ackdif algetx comzge conlou coudrt diskva 
2923be93fa7f -> accxvc ackytd algxiz comgzg conqrw couyov disvkc 
```

...the obvious winner being the third one. We can now see that the SMS words
are similar to `accomplish`, `acknowledge`, `algorithm`, `compatible`,
`confidential`, `disappointing` for x starting with `\x29\x23\xbe\x84\xe1\x6c`.

The next step is doing the same procedure knowing the first 6 characters of x:

```py
for x6 in possible_x[6]:
	for x7 in possible_x[7]:
		for x8 in possible_x[8]:
			X = "\x29\x23\xbe\x84\xe1\x6c"+x6+x7+x8
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(9)]) + " ")
```

Even though there still are lots of possible combinations we now know what to
look for and can search for the words we figured out earlier:

```
2923be84e16cd6ae52 -> accomplis acknowled algorithm compatibi confident counterex disappoin 
```

Doing this two more times gets us the decrypted sms messages and x:
```
2923be84e16cd6ae529049f1f1bbe9 -> accomplishments acknowledgement algorithmically compatibilities confidentiality counterexamples disappointingly 
```

The flag is x but with uppercase letters. It does conform with the "x is all
uppercase and digits" description but in a bad way :p

```
ECSC{2923BE84E16CD6AE529049F1F1BBE9}
```
>>>>>>> master
