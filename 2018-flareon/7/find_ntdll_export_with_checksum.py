def checksum(a):
	cs = 0x00000000
	for c in a:
		cs = ((((cs << 16) & 0xffff0000) | ((cs >> 16) & 0x0000ffff)) + ord(c) + 10) & 0xffffffff
	return cs

print(checksum("n\x00t\x00d\x00l\x00l\x00.\x00d\x00l\x00l\x00"))

lines = open("ntdllexports.txt", "r").read().split("\n")
for line in lines:
	line = line.strip()
	if checksum(line) == 0x57405C3:
		print(line)