u32 = 0xffffffff

def getA(x):
	x = ((x * 525120127) ^ -1857011168) & u32
	t = x >> 30
	print(["array", "string", "object", "ERR"][t])

def getB(x):
	x = ((x * -92057199) ^ 12411463) & u32
	t = x >> 30
	print(["object", "string", "ERR", "array"][t])

def getC(x):
	x = ((x * 58189191) ^ 203060217) & u32
	t = x >> 30
	print(["object", "array", "ERR", "string"][t])

def getD(x):
	x = ((x * -1872265651) ^ 2098719435) & u32
	t = x >> 30
	print(["ERR", "array", "string", "object"][t])

getA(4157369824)
getA(2479234387)
getA(181887182)
getA(2000007406)

getB(4024023210)
getB(3304882116)
getB(2396514616)
getB(2240091216)

getC(475005910)
getC(4064174717)
getC(2599456470)

getD(1821626616)
getD(3162600228)

# YAY!
