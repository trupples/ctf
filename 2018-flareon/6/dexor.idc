#include <idc.idc>

static xor(fun, key, len) {
	auto i;
	for(i=0; i<len; i++) {
		PatchByte(fun+i, Byte(fun+i) ^ Byte(key+i));
	}
}

static dx(i) {
	auto e = get_name_ea_simple("thingyBuf") + i * 288;
	auto fun = Qword(e), key = Qword(e+0x18), len = Dword(e+8);
	print(fun, key, len);	
}

static dexorEntry(i) {
	auto e = get_name_ea_simple("thingyBuf") + i * 288;
	auto fun = Qword(e), key = Qword(e+0x18), len = Dword(e+8);
	xor(fun, key, len);
	add_func(fun);
}

static rexorEntry(i) {
	auto e = get_name_ea_simple("thingyBuf") + i * 288;
	auto fun = Qword(e), key = Qword(e+0x18), len = Dword(e+8);
	xor(fun, key, len);
	del_func(fun);
}
