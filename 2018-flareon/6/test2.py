import struct

fiblookup = [0] * 256
for i in range(256):
	v6 = 0
	v5 = 1
	v4 = 0
	ci = i
	while ci != 0:
		v6 = (v5 + v4)
		v4 = v5
		v5 = v6
		ci -= 1
	#v6 = (v6 & 0x7fffffffffffffff) - 2*(v6 & 0x8000000000000000)
	v6 = v6 & 0xffffffffffffffff
	fiblookup[i] = v6

def rfib(x):
	for i in range(256):
		if fiblookup[i] == x:
			return i
	print("NOPE")

correct = ['_']*69	# xddd

# Entry 0
correct[2] = chr(rfib(0x12062F76909038C5))
correct[3] = chr(rfib(0x0EC9B9E4287BCE2D))
correct[4] = chr(rfib(0x35C7E2))

# Entry 1
e1_numbers = struct.unpack("LL", b'\xF7\x84\x69\x60\xFB\xCC\x7A\x93b\xEF\x18\xB5C\xFELh')
correct[0x2c] = chr(rfib(e1_numbers[0]))
correct[0x2d] = chr(rfib(e1_numbers[1]))

# Entry 2
"""
#include <stdio.h>
int main() {
	unsigned char x=0;
	do {
		unsigned int v4 = 0xFFFFFFFF ^ x;
		for(int i=7; i>=0; i--) {
			v4 = (v4 >> 1) ^ -(v4&1) & 0xEDB88320;
		}
		if(~v4 == (('B') | (0xE2<<8) | (0xD4<<16) | (0x0E<<24)))
			printf("< %d\n", x);
		if(~v4 == (('B'<<24) | (0xE2<<16) | (0xD4<<8) | (0x0E)))
			printf("> %d\n", x);
	} while(++x != 0);

	printf("Nope :/");
	return 0;
}

Couldn't quite get non-bignum arithmetic right in python
"""
correct[16] = chr(46)

# Entry 3
"""
#include <stdio.h>
int main() {
	char data[3] = {0xad, '-', 0x84};
	unsigned char xorkey[256], x=0, v8=0,v9=0;

	for(int i=0; i<256; i++) xorkey[i]=i;
	for(int i=0; i<256; i++) {
		x += xorkey[i] + "Tis but a scratch."[i%18];
		xorkey[i] ^= xorkey[x];
		xorkey[x] ^= xorkey[i];
		xorkey[i] ^= xorkey[x];
	}
	for(int i=0; i<3; i++) {
		v8+=xorkey[++v9];
		xorkey[v8] ^= xorkey[v9];
		xorkey[v9] ^= xorkey[v8];
		xorkey[v8] ^= xorkey[v9];
		x = xorkey[v8] + xorkey[v9];
		printf("%c", data[i] ^ xorkey[x]);
	}

	printf("\n");
	return 0;
}
"""
correct[7] = 'n'
correct[8] = 'g'
correct[9] = ' '

# Entry 4
correct[0x3f] = ' '	# Need to figure this one out

# Entry 5
e5_numbers = struct.unpack("LLL", b'"[\x8f#HQ\xb1\xf5\x9b\x99\xd6\x01\x05\xf3\xc6\x2a\xc2y1\x98z\x8f\xb8_')
correct[0x39] = chr(rfib(e5_numbers[0]))
correct[0x3a] = chr(rfib(e5_numbers[1]))
correct[0x3b] = chr(rfib(e5_numbers[2]))

# Entry 6
e6_numbers = struct.unpack("LL", b'\x88\xF8$V\x1E\xA6\xE1\x45\xE2\xC7\x35\x00\x00\x00\x00\x00')
correct[0x30] = chr(rfib(e6_numbers[0]))
correct[0x31] = chr(rfib(e6_numbers[1]))

# Entry 7
correct[0x1e] = chr(0x81 - 13)
correct[0x1f] = chr(ord('u') - 13)
correct[0x20] = chr(ord('r') - 13)

# Entry 8
correct[0xa] = ' '

# Entry 9
correct[0x3c] = ' '
correct[0x3d] = 'i'
correct[0x3e] = 'n'

# Entry 10
correct[0xd] = chr(ord('t') - 13)

# Entry 11
correct[0x2b] = chr(rfib(0x35c7e2))

# Entry 12
correct[0x38] = chr(0x7f - 13)

# Entry 13
correct[0x12] = chr(ord('O') ^ 0x2a)
correct[0x13] = chr(0x0a ^ 0x2a)

# Entry 14
correct[0x11] = ' '

# Entry 15
"""
Similar to #2

#include <stdio.h>
int main() {
	unsigned char x=0;
	do {
		unsigned char y=0;
		do {
			unsigned char z=0;	
			do {
				unsigned int v4 = 0xFFFFFFFF;
				v4 = v4 ^ x;
				for(int i=7; i>=0; i--) {
					v4 = (v4 >> 1) ^ -(v4&1) & 0xEDB88320;
				}
				v4 = v4 ^ y;
				for(int i=7; i>=0; i--) {
					v4 = (v4 >> 1) ^ -(v4&1) & 0xEDB88320;
				}
				v4 = v4 ^ z;
				for(int i=7; i>=0; i--) {
					v4 = (v4 >> 1) ^ -(v4&1) & 0xEDB88320;
				}
				if(~v4 == (0xB6 | (0xD5<<8) | (0x5C<<16) | (0xC5<<24)))
					printf("< %d %d %d '%c%c%c'\n", x, y, z, x, y, z);
				if(~v4 == ((0xB6<<24) | (0xD5<<16) | (0x5C<<8) | (0xC5)))
					printf("> %d %d\n", x, y);
			} while(++z != 0);
		} while(++y != 0);
	} while(++x != 0);

	printf("Nope :/");
	return 0;
}
"""
correct[0x42] = 'a'
correct[0x43] = 'c'
correct[0x44] = 'e'


# Entry 16
correct[0x28] = 'l'
correct[0x29] = 'i'
correct[0x2a] = 'k'

# Entry 17
correct[0x2e] = chr(ord('O') ^ 0x2a)

# Entry 18
correct[0] = chr(0x76 - 13)
correct[1] = chr(0x7b - 13)

# Entry 19
# Using the c program from entry #3
correct[0xb] = "l"
correct[0xc] = "l"

# Entry 20 
correct[0x26] = chr(ord('D') ^ 0x2a)
correct[0x27] = chr(ord('E') ^ 0x2a)

# Entry 21
correct[0x14] = chr(0x0a ^ 0x2a)
correct[0x15] = chr(ord('b') ^ 0x2a)

# Entry 22
# One char bruteforce
correct[0x32] = ' '
correct[0x33] = 'y'
correct[0x34] = 'o'

# Entry 23
correct[0x24] = chr(0x82 - 13)
correct[0x25] = chr(0x7f - 13)

# Entry 24
correct[0x1c] = 'o'
correct[0x1d] = 'f'

# Entry 25
correct[0x2f] = 'f'

# Entry 26
correct[5] = chr(ord('C') ^ 0x2a)
correct[6] = chr(ord('Y') ^ 0x2a)

# Entry 27
correct[0x35] = chr(0x0a ^ 0x2a)
correct[0x36] = chr(ord('H') ^ 0x2a)
correct[0x37] = chr(ord('F') ^ 0x2a)

# Entry 28
correct[0x40] = chr(rfib(0x2ac6f30501d6999b))
correct[0x41] = chr(rfib(struct.unpack("L", b"\xA0\x09\x8c\x198\"\xbd<")[0]))


# Entry 29
correct[0x16] = chr(ord('^') ^ 0x2a)
correct[0x17] = chr(ord('B') ^ 0x2a)
correct[0x18] = chr(ord('C') ^ 0x2a)

# Entry 30
correct[0xe] = chr(rfib(0x35c7e2))
correct[0xf] = chr(rfib(struct.unpack("L", b"\xA0\x09\x8c\x198\"\xbd<")[0]))

# Entry 31
correct[0x19] = chr(0x81 - 13)
correct[0x1a] = chr(ord('u') - 13)
correct[0x1b] = chr(ord('r') - 13)

# Entry 32
correct[0x21] = chr(ord('k') ^ 0x2a)
correct[0x22] = chr(ord('B') ^ 0x2a)
correct[0x23] = chr(6 ^ 0x2a)


print("".join(correct))