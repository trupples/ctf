<<<<<<< HEAD
[Go back](.)

### Watch your head - 50 points

Accessing the [website](http://89.38.210.129:8015/) we're given we are presented
with a simple webpage containing an image. As the task title might suggest,
there's important data in the HTTP headers that come with the server's response.
All responses had an X-Data header and a Set-Cookie one that set the Data-X
cookie. These two values changed every time the page was reloaded, so I gathered
a bunch:

 Response no | X-Data | Data-X cookie | Response no | X-Data | Data-X cookie
:-----------:|:------:|:-------------:|:-----------:|:------:|:-------------:
1            |`t`     |`i`            |12           |`t`     |`c`
2            |`m`     |`c`            |13           |`h`     |`3`
3            |`t`     |`f`            |14           |`d`     |`_`
4            |`{`     |`c`            |15           |`y`     |`0`
5            |`0`     |`n`            |16           |`u`     |`r`
6            |`g`     |`r`            |17           |`_`     |`h`
7            |`4`     |`t`            |18           |`3`     |`4`
8            |`s`     |`_`            |19           |`D`     |`D`
9            |`y`     |`0`            |20           |`D`     |`_`
10           |`u`     |`_`            |21           |`_`     |`}`
11           |`w`     |`4`            |22           |`t`     |`i`

=======
[Go back](.)

### Watch your head - 50 points

Accessing the [website](http://89.38.210.129:8015/) we're given we are presented
with a simple webpage containing an image. As the task title might suggest,
there's important data in the HTTP headers that come with the server's response.
All responses had an X-Data header and a Set-Cookie one that set the Data-X
cookie. These two values changed every time the page was reloaded, so I gathered
a bunch:

 Response no | X-Data | Data-X cookie | Response no | X-Data | Data-X cookie
:-----------:|:------:|:-------------:|:-----------:|:------:|:-------------:
1            |`t`     |`i`            |12           |`t`     |`c`
2            |`m`     |`c`            |13           |`h`     |`3`
3            |`t`     |`f`            |14           |`d`     |`_`
4            |`{`     |`c`            |15           |`y`     |`0`
5            |`0`     |`n`            |16           |`u`     |`r`
6            |`g`     |`r`            |17           |`_`     |`h`
7            |`4`     |`t`            |18           |`3`     |`4`
8            |`s`     |`_`            |19           |`D`     |`D`
9            |`y`     |`0`            |20           |`D`     |`_`
10           |`u`     |`_`            |21           |`_`     |`}`
11           |`w`     |`4`            |22           |`t`     |`i`

>>>>>>> master
The flag is `timctf{c0ngr4ts_y0u_w4tch3d_y0ur_h34DDD__}`