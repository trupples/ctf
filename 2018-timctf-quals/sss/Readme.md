<<<<<<< HEAD
[Go back](..)

### SSS Part 1 - 75 points

The given image illustrated a linear graph on which 3 points' coordinates are given in base 16:

```
f(1) = 0x4612c90f5d8cd5d616193257336d92af1f66df92443b4ee69f5c885f0173ad80113844e393d194e3
f(2) = 0x8c25921e46b03e48b7cbe94c3267f41adf618abd16422f660b59df6fae81e8aff2242852be33db49
f(3) = 0xd2385b2d2fd3a6bb597ea041316255869f5c35e7e8490fe5775736805b9023dfd3100bc1e89621af
```

A point on the y axis is highlighted. That point's y coordinate is the flag.

```py
import binascii

a = 0x4612c90f5d8cd5d616193257336d92af1f66df92443b4ee69f5c885f0173ad80113844e393d194e3
b = 0x8c25921e46b03e48b7cbe94c3267f41adf618abd16422f660b59df6fae81e8aff2242852be33db49
c = 0xd2385b2d2fd3a6bb597ea041316255869f5c35e7e8490fe5775736805b9023dfd3100bc1e89621af

# f(1) = a
# f(2) = b
# f(3) = c

# Linearity check:
assert(a - b == b - c)

# f(0) = secret
# a = (secret + b) / 2
# 2 * a = secret + b
# secret = 2 * a - b

secret = 2 * a - b

print binascii.unhexlify(hex(secret)[2:-1]) # => timctf{b4s1C_l4gr4ng3_1NTerP0LatioN}
```

This task was a very simple example of [Shamir's Secret Sharing algorithm](//wikipedia.org/wiki/Shamir's_Secret_Sharing) (hence the name) that used a degree 1 polynomial, thus requiring knowledge of 2 points to solve.
=======
[Go back](..)

### SSS Part 1 - 75 points

The given image illustrated a linear graph on which 3 points' coordinates are given in base 16:

```
f(1) = 0x4612c90f5d8cd5d616193257336d92af1f66df92443b4ee69f5c885f0173ad80113844e393d194e3
f(2) = 0x8c25921e46b03e48b7cbe94c3267f41adf618abd16422f660b59df6fae81e8aff2242852be33db49
f(3) = 0xd2385b2d2fd3a6bb597ea041316255869f5c35e7e8490fe5775736805b9023dfd3100bc1e89621af
```

A point on the y axis is highlighted. That point's y coordinate is the flag.

```py
import binascii

a = 0x4612c90f5d8cd5d616193257336d92af1f66df92443b4ee69f5c885f0173ad80113844e393d194e3
b = 0x8c25921e46b03e48b7cbe94c3267f41adf618abd16422f660b59df6fae81e8aff2242852be33db49
c = 0xd2385b2d2fd3a6bb597ea041316255869f5c35e7e8490fe5775736805b9023dfd3100bc1e89621af

# f(1) = a
# f(2) = b
# f(3) = c

# Linearity check:
assert(a - b == b - c)

# f(0) = secret
# a = (secret + b) / 2
# 2 * a = secret + b
# secret = 2 * a - b

secret = 2 * a - b

print binascii.unhexlify(hex(secret)[2:-1]) # => timctf{b4s1C_l4gr4ng3_1NTerP0LatioN}
```

This task was a very simple example of [Shamir's Secret Sharing algorithm](//wikipedia.org/wiki/Shamir's_Secret_Sharing) (hence the name) that used a degree 1 polynomial, thus requiring knowledge of 2 points to solve.
>>>>>>> master
