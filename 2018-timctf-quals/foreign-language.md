<<<<<<< HEAD
[Go back](.)

### Foreign Language - 150 points

The windows executable is a py2exe compiled python script. I used unpy2exe
on a python 3.4 installation (that's the same as the version the executable was compiled
on) to get a .pyc file and then used uncompyle to get some readable python
code:

```py
# uncompyle6 version 3.1.2
# Python bytecode 3.4 (3310)
# Decompiled from: Python 3.4.0 (v3.4.0:04f714765c13, Mar 16 2014, 19:25:23) [MSC v.1600 64 bit (AMD64)]
# Embedded file name: haforli.py
# Compiled at: 2018-04-14 20:57:32
# Size of source mod 2**32: 198 bytes
if 0:
    i11iIiiIii
import sys, random, string
if 0:
    O0 / iIii1I11I1II1 % OoooooooOO - i1IIi
o0OO00 = "\n\n              __            _ _ \n  /\\  /\\__ _ / _| ___  _ __| (_)\n / /_/ / _` | |_ / _ \\| '__| | |\n/ __  / (_| |  _| (_) | |  | | |\n\\/ /_/ \\__,_|_|  \\___/|_|  |_|_|\n                                \n"
if 0:
    i11i.oOooOoO0Oo0O
if 0:
    IIiI1I11i11
ooOO00oOo = --- This code section failed: ---

  18       0  LOAD_GLOBAL              'len'
           3  LOAD_FAST                'OOOo0'
           6  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
           9  LOAD_CONST            2  2
          12  BINARY_MODULO    
          13  LOAD_CONST            1  1
          16  COMPARE_OP               '=='
          19  POP_JUMP_IF_FALSE    26  'to 26'
          22  LOAD_CONST            1  True
          25  RETURN_END_IF_LAMBDA
          26  LOAD_CONST            0  False
          29  RETURN_VALUE_LAMBDA
          -1  LAMBDA_MARKER    

Parse error at or near `None' instruction at offset -1
if 0:
    i1 - o0 * i1oOo0OoO * iIIIiiIIiiiIi % Oo

def o0O():
    print('Input an English word to be translated into a Haforli word: ')
    IiiIII111iI = input().strip().split(' ')[0]
    if 0:
        iii1I1I / O00oOoOoO0o0O.O0oo0OO0 + Oo0ooO0oo0oO.I1i1iI1i - II
    OoI1Ii11I1Ii1i = IiiIII111iI[::-1]
    Ooo = ''
    o0oOoO00o = len(OoI1Ii11I1Ii1i) - 1 if ooOO00oOo(OoI1Ii11I1Ii1i) else len(OoI1Ii11I1Ii1i)
    for i1oOOoo00O0O in range(0, o0oOoO00o, 2):
        Ooo += OoI1Ii11I1Ii1i[i1oOOoo00O0O + 1] + OoI1Ii11I1Ii1i[i1oOOoo00O0O]
        if 0:
            I11iii11IIi
            continue

    if ooOO00oOo(OoI1Ii11I1Ii1i):
        Ooo += OoI1Ii11I1Ii1i[-1]
        if 0:
            Oo0ooO0oo0oO * i11iIiiIii * oOooOoO0Oo0O % Oo0ooO0oo0oO * Oo0ooO0oo0oO * i11i
        o0o0Oo0oooo0 = ''
        for oO0O0o0o0 in Ooo:
            if not oO0O0o0o0.isalpha():
                o0o0Oo0oooo0 += oO0O0o0o0 + oO0O0o0o0
            elif oO0O0o0o0.isupper():
                o0o0Oo0oooo0 += oO0O0o0o0 + random.choice(string.ascii_uppercase)
            else:
                o0o0Oo0oooo0 += oO0O0o0o0 + random.choice(string.ascii_lowercase)
                if 0:
                    i1
                    continue

        print(('Your translated word: {0}').format(o0o0Oo0oooo0))
    if 0:
        i1oOo0OoO - IIiI1I11i11
    if 0:
        i11iIiiIii % II


if __name__ == '__main__':
    print(o0OO00)
    o0O()
```

It failed to decompile one function but it seems like it was only a check that
isn't *that* relevant to solving the challenge. I refactored some of the
variables to understand the code better and got to this:

```py
def main():
    print('Input an English word to be translated into a Haforli word: ')
    englishword = input().strip().split(' ')[0]

    # <<<< 1 >>>>
    reversedinput = englishword[::-1]
    pairsflipped = ''

    # <<<< 2 >>>>
    last_odd_index = len(reversedinput) - 1 if bad_function(reversedinput) else len(reversedinput)
    for index in range(0, last_odd_index, 2):
        pairsflipped += reversedinput[index + 1] + reversedinput[index]

    if bad_function(reversedinput):
        pairsflipped += reversedinput[-1]
        result = ''
        # <<<< 3 >>>>
        for c in pairsflipped:
            if not c.isalpha():
                result += c + c
            elif c.isupper():
                result += c + random.choice(string.ascii_uppercase)
            else:
                result += c + random.choice(string.ascii_lowercase)

        print(('Your translated word: {0}').format(result))
```

The code reverses the input string (<<<< 1 >>>>) and then flips each character
pair (<<<< 2 >>>>), so, for example, ABCDEFGHIJKLMNOP would become
PONMLKJIHGFEDCBA and then OPMNKLIJGHEFCDAB. After that it appends (<<<< 3 >>>>)
a random letter after each letter in the reversed and flipped string and
duplicates all non-letter characters.

To reverse this we can ignore all odd-indexed characters in the "encrypted"
string to get

d}rlw0y__mofs_1t1m_lhe_tr3_ag3u4ngl4y__mofs_1t1m{ltfmcti

then flip all consecutive character pairs to get

}dlr0w_ym_fo_st1m1l_eht_3ra_3g4ugn4l_ym_fo_st1m1l{ftcmit

and reversing that gets us the flag:

timctf{l1m1ts_of_my_l4ngu4g3_ar3_the_l1m1ts_of_my_w0rld}
=======
[Go back](.)

### Foreign Language - 150 points

The windows executable is a py2exe compiled python script. I used unpy2exe
on a python 3.4 installation (that's the same as the version the executable was compiled
on) to get a .pyc file and then used uncompyle to get some readable python
code:

```py
# uncompyle6 version 3.1.2
# Python bytecode 3.4 (3310)
# Decompiled from: Python 3.4.0 (v3.4.0:04f714765c13, Mar 16 2014, 19:25:23) [MSC v.1600 64 bit (AMD64)]
# Embedded file name: haforli.py
# Compiled at: 2018-04-14 20:57:32
# Size of source mod 2**32: 198 bytes
if 0:
    i11iIiiIii
import sys, random, string
if 0:
    O0 / iIii1I11I1II1 % OoooooooOO - i1IIi
o0OO00 = "\n\n              __            _ _ \n  /\\  /\\__ _ / _| ___  _ __| (_)\n / /_/ / _` | |_ / _ \\| '__| | |\n/ __  / (_| |  _| (_) | |  | | |\n\\/ /_/ \\__,_|_|  \\___/|_|  |_|_|\n                                \n"
if 0:
    i11i.oOooOoO0Oo0O
if 0:
    IIiI1I11i11
ooOO00oOo = --- This code section failed: ---

  18       0  LOAD_GLOBAL              'len'
           3  LOAD_FAST                'OOOo0'
           6  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
           9  LOAD_CONST            2  2
          12  BINARY_MODULO    
          13  LOAD_CONST            1  1
          16  COMPARE_OP               '=='
          19  POP_JUMP_IF_FALSE    26  'to 26'
          22  LOAD_CONST            1  True
          25  RETURN_END_IF_LAMBDA
          26  LOAD_CONST            0  False
          29  RETURN_VALUE_LAMBDA
          -1  LAMBDA_MARKER    

Parse error at or near `None' instruction at offset -1
if 0:
    i1 - o0 * i1oOo0OoO * iIIIiiIIiiiIi % Oo

def o0O():
    print('Input an English word to be translated into a Haforli word: ')
    IiiIII111iI = input().strip().split(' ')[0]
    if 0:
        iii1I1I / O00oOoOoO0o0O.O0oo0OO0 + Oo0ooO0oo0oO.I1i1iI1i - II
    OoI1Ii11I1Ii1i = IiiIII111iI[::-1]
    Ooo = ''
    o0oOoO00o = len(OoI1Ii11I1Ii1i) - 1 if ooOO00oOo(OoI1Ii11I1Ii1i) else len(OoI1Ii11I1Ii1i)
    for i1oOOoo00O0O in range(0, o0oOoO00o, 2):
        Ooo += OoI1Ii11I1Ii1i[i1oOOoo00O0O + 1] + OoI1Ii11I1Ii1i[i1oOOoo00O0O]
        if 0:
            I11iii11IIi
            continue

    if ooOO00oOo(OoI1Ii11I1Ii1i):
        Ooo += OoI1Ii11I1Ii1i[-1]
        if 0:
            Oo0ooO0oo0oO * i11iIiiIii * oOooOoO0Oo0O % Oo0ooO0oo0oO * Oo0ooO0oo0oO * i11i
        o0o0Oo0oooo0 = ''
        for oO0O0o0o0 in Ooo:
            if not oO0O0o0o0.isalpha():
                o0o0Oo0oooo0 += oO0O0o0o0 + oO0O0o0o0
            elif oO0O0o0o0.isupper():
                o0o0Oo0oooo0 += oO0O0o0o0 + random.choice(string.ascii_uppercase)
            else:
                o0o0Oo0oooo0 += oO0O0o0o0 + random.choice(string.ascii_lowercase)
                if 0:
                    i1
                    continue

        print(('Your translated word: {0}').format(o0o0Oo0oooo0))
    if 0:
        i1oOo0OoO - IIiI1I11i11
    if 0:
        i11iIiiIii % II


if __name__ == '__main__':
    print(o0OO00)
    o0O()
```

It failed to decompile one function but it seems like it was only a check that
isn't *that* relevant to solving the challenge. I refactored some of the
variables to understand the code better and got to this:

```py
def main():
    print('Input an English word to be translated into a Haforli word: ')
    englishword = input().strip().split(' ')[0]

    # <<<< 1 >>>>
    reversedinput = englishword[::-1]
    pairsflipped = ''

    # <<<< 2 >>>>
    last_odd_index = len(reversedinput) - 1 if bad_function(reversedinput) else len(reversedinput)
    for index in range(0, last_odd_index, 2):
        pairsflipped += reversedinput[index + 1] + reversedinput[index]

    if bad_function(reversedinput):
        pairsflipped += reversedinput[-1]
        result = ''
        # <<<< 3 >>>>
        for c in pairsflipped:
            if not c.isalpha():
                result += c + c
            elif c.isupper():
                result += c + random.choice(string.ascii_uppercase)
            else:
                result += c + random.choice(string.ascii_lowercase)

        print(('Your translated word: {0}').format(result))
```

The code reverses the input string (<<<< 1 >>>>) and then flips each character
pair (<<<< 2 >>>>), so, for example, ABCDEFGHIJKLMNOP would become
PONMLKJIHGFEDCBA and then OPMNKLIJGHEFCDAB. After that it appends (<<<< 3 >>>>)
a random letter after each letter in the reversed and flipped string and
duplicates all non-letter characters.

To reverse this we can ignore all odd-indexed characters in the "encrypted"
string to get

d}rlw0y__mofs_1t1m_lhe_tr3_ag3u4ngl4y__mofs_1t1m{ltfmcti

then flip all consecutive character pairs to get

}dlr0w_ym_fo_st1m1l_eht_3ra_3g4ugn4l_ym_fo_st1m1l{ftcmit

and reversing that gets us the flag:

timctf{l1m1ts_of_my_l4ngu4g3_ar3_the_l1m1ts_of_my_w0rld}
>>>>>>> master
