targetString = "@25-Q44E233=,>E-M34=,,$LS5VEQ45)M2S-),7-$/3T "

decodedBits = ""
for c in targetString[1:]:
	decodedBits += bin(ord(c) - ord(' ') + 64)[-6:] 
decodedText = ""
for i in range(0, len(decodedBits), 8):
	decodedText += chr(int(decodedBits[i:i+8], 2))
print(decodedText)


rot13edText = ""
for c in decodedText:
	if ord(c) >= ord('a') and ord(c) <= ord('z'):
		rot13edText += chr((ord(c) - ord('a') + 13) % 26 + ord('a'))
	elif ord(c) >= ord('A') and ord(c) <= ord('Z'):
		rot13edText += chr((ord(c) - ord('A') + 13) % 26 + ord('A'))
	else:
		rot13edText += c
print(rot13edText)


from base64 import b64decode
print(b64decode(rot13edText))