import sys
from string import printable as alphabet

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 _!}{"

assert(len(sys.argv) == 2)

def p(l, n):
	if l >= n:
		return [""]

	passwords = []
	nextlevel = p(l+1, n)
	for c in alphabet:
		for nl in nextlevel:
			passwords += [c + nl]

	return passwords


passwords = p(0, int(sys.argv[1]))
for p in passwords:
	print p