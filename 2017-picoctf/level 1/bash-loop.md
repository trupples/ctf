[Go back](./Readme.md)

### Bash loop - 40 points

As the task name suggests, I can use a bash for loop to go through all values from 0 to 4096, appending the program's result to a file in my home directory. Using `grep` I filtered out (`-v` option) all the lines containing "Nope", thus getting the flag:

```
trupples@shell-web:/problems/1341ae866a3bcee40a916de97678a0a1$ for i in {0..4096}; do ./bashloop $i >> ~/bashloop.out; done;
trupples@shell-web:/problems/1341ae866a3bcee40a916de97678a0a1$ grep -v Nope ~/bashloop.out
Yay! That's the number! Here be the flag: 460b1c7b20d753cfdc17f0d4b6a01dae
```
