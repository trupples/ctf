[Go back](./Readme.md)

### Leaf of the Forest - 30 points

Just like in the second one, but now I'm using `grep` so as to avoid manually searching through the possibly hundreds of lines:

```
trupples@shell-web:/problems/df79496e08066eab7b27611c897c4151$ find . | grep flag
./forest/tree776de2/trunkf1d0/trunk2e70/trunk427b/trunk05c3/trunka801/trunk878b/trunkd3d6/branchccd0/flag
trupples@shell-web:/problems/df79496e08066eab7b27611c897c4151$ cat ./forest/tree776de2/trunkf1d0/trunk2e70/trunk427b/trunk05c3/trunka801/trunk878b/trunkd3d6/branchccd0/flag
54a57e77602a964676e0d11da4074737
```
