<<<<<<< HEAD
[Go back](./Readme.md)

### TW_GR_E2_EoTDS - 120 points

The server files looked similar to the previous ones so I just played the game to see how it goes. Given the hint, I first tried to see if the protocol specifies the entity id of the moving entity to try to use that to control the spatula, but that wasn't the case.

The culprit is that `server/ai.js` doesn't do pathfinding. If the player is in the same room as the enemy, it just moves to the one of the valid tiles next to it that's closest to the player. Using that I guided the spatula through the maze to the flag and then outside. Once the spatula was outside I killed it to get the flag it picked up, and upon using the item I was presented with the challenge flag. One thing to keep in mind, though, is that if you don't go diagonally on all the corners of the last level, the spatula will get too close to you and this won't work.

```
Toaster used the Flag!
A soft voice on the wind speaks to you: "The secret you are looking for is i_dont_want_to_say_goodbye__gets_me_every_time_3a3d7180ec0eb608d28a01e6d45f6ea2. Use it wisely."
```
=======
[Go back](./Readme.md)

### TW_GR_E2_EoTDS - 120 points

The server files looked similar to the previous ones so I just played the game to see how it goes. Given the hint, I first tried to see if the protocol specifies the entity id of the moving entity to try to use that to control the spatula, but that wasn't the case.

The culprit is that `server/ai.js` doesn't do pathfinding. If the player is in the same room as the enemy, it just moves to the one of the valid tiles next to it that's closest to the player. Using that I guided the spatula through the maze to the flag and then outside. Once the spatula was outside I killed it to get the flag it picked up, and upon using the item I was presented with the challenge flag. One thing to keep in mind, though, is that if you don't go diagonally on all the corners of the last level, the spatula will get too close to you and this won't work.

```
Toaster used the Flag!
A soft voice on the wind speaks to you: "The secret you are looking for is i_dont_want_to_say_goodbye__gets_me_every_time_3a3d7180ec0eb608d28a01e6d45f6ea2. Use it wisely."
```
>>>>>>> master
