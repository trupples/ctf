<<<<<<< HEAD
[Go back](./Readme.md)

## Master Challenge - Missing Identity - 100 points

The file has the first 6 bytes X-ed out, so its magic number (the "identity" in the title) must be guessed. The first thing after a few header bytes is a filename `flag.png` and that file's contents. 

```
$ hexdump -C file | head
00000000  58 58 58 58 58 58 00 00  08 00 23 44 7f 4a 68 d5  |XXXXXX....#D.Jh.|
00000010  28 00 d1 8d 00 00 c2 8d  00 00 08 00 00 00 66 6c  |(.............fl|
00000020  61 67 2e 70 6e 67 00 64  40 9b bf 89 50 4e 47 0d  |ag.png.d@...PNG.|
00000030  0a 1a 0a 00 00 00 0d 49  48 44 52 00 00 02 54 00  |.......IHDR...T.|
00000040  00 00 3c 08 02 00 00 00  2f c2 b3 60 00 00 8d 89  |..<...../..`....|
00000050  49 44 41 54 78 9c ec fd  57 8f 25 59 96 26 8a ad  |IDATx...W.%Y.&..|
00000060  b5 b7 e9 63 47 b9 0e f7  d0 3a 23 43 a4 aa ca 12  |...cG....:#C....|
00000070  d9 95 5d dd 3d 5d 7d 7b  e6 ce 70 1a 03 f2 95 c3  |..].=]}{..p.....|
00000080  07 f2 fe 00 82 00 f9 72  01 82 00 09 70 1e 08 12  |.......r....p...|
00000090  b8 e4 cb 05 88 01 08 82  60 8f b8 62 a6 bb ab 67  |........`..b...g|
```

Based on that this is probably an archive file, so I tried to replace the X-es with common archive magic numbers. Luckily, the first one I tried - `PK\x03\x04\x00\x00` for a zip archive - worked:

![Screenshot of the archive and flag image](master.png)

So the flag is `zippidydooda44282245`
=======
[Go back](./Readme.md)

## Master Challenge - Missing Identity - 100 points

The file has the first 6 bytes X-ed out, so its magic number (the "identity" in the title) must be guessed. The first thing after a few header bytes is a filename `flag.png` and that file's contents. 

```
$ hexdump -C file | head
00000000  58 58 58 58 58 58 00 00  08 00 23 44 7f 4a 68 d5  |XXXXXX....#D.Jh.|
00000010  28 00 d1 8d 00 00 c2 8d  00 00 08 00 00 00 66 6c  |(.............fl|
00000020  61 67 2e 70 6e 67 00 64  40 9b bf 89 50 4e 47 0d  |ag.png.d@...PNG.|
00000030  0a 1a 0a 00 00 00 0d 49  48 44 52 00 00 02 54 00  |.......IHDR...T.|
00000040  00 00 3c 08 02 00 00 00  2f c2 b3 60 00 00 8d 89  |..<...../..`....|
00000050  49 44 41 54 78 9c ec fd  57 8f 25 59 96 26 8a ad  |IDATx...W.%Y.&..|
00000060  b5 b7 e9 63 47 b9 0e f7  d0 3a 23 43 a4 aa ca 12  |...cG....:#C....|
00000070  d9 95 5d dd 3d 5d 7d 7b  e6 ce 70 1a 03 f2 95 c3  |..].=]}{..p.....|
00000080  07 f2 fe 00 82 00 f9 72  01 82 00 09 70 1e 08 12  |.......r....p...|
00000090  b8 e4 cb 05 88 01 08 82  60 8f b8 62 a6 bb ab 67  |........`..b...g|
```

Based on that this is probably an archive file, so I tried to replace the X-es with common archive magic numbers. Luckily, the first one I tried - `PK\x03\x04\x00\x00` for a zip archive - worked:

![Screenshot of the archive and flag image](master.png)

So the flag is `zippidydooda44282245`
>>>>>>> master
