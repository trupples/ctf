<<<<<<< HEAD
"""
# Censorship part 1: Proof of Work - 50 points

I just read the first 12 chars and repeatedly append a suffix and check whether
the hash of the challenge+suffix meets the requirements (last 3 bytes are all
0xff). When a suffix that yields a good hash is found I send it to to the
server and get the part I flag.

This should take 16777216 iterations on average (somewhere around 30-40s on
my machine)

"""

from hashlib import sha1
import socket
import re

nc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
nc.connect(("45.76.89.32", 1234))
nc.settimeout(1)

challenge = nc.recv(12)
print challenge
suffix = 0
while True:
    proof = challenge + str(suffix)
    if sha1(proof).hexdigest().endswith("ffffff"):
        print(proof)
        nc.send(proof + "\n")
        break
    suffix = suffix + 1

# Part 1 flag
print nc.recv(64)

"""
# Censorship part 2: RSA ownage    - 100 points

t - the flag including the appended/preppended random chars
C - the encrypted flag
N, e, d - textbook RSA parameters

C = t^e (mod N)

d is coprime to LCM(p-1, q-1). p-1 and q-1 are even values because p and q
are primes greater than 2. The LCM of two even values must be also even so d
must be odd. Therefore (-1)^d will equal -1, which is congruent to
N-1 modulo N. I can calculate N by requesting (-1)^d and adding 1 to the
response value.

N = 1 + request(-1)                <--- first request

let C' be the modular multiplicative inverse of C modulo N

C' = C^(-1) mod N

then

C'^d = inverse(C)^d = inverse(C^d) = inverse(t)
t = inverse(C')
t = inverse(request(inverse(C)))    <--- second request

After this I used the first web hex to ascii converter I found (too lazy to
make one) to get the flag

"""

# Part 2 first message
prompt = nc.recv(1024)
C = int(re.search("\?\s(\d+)", prompt).group(1))
print "C =", C

# Calculate N = request(-1) + 1
nc.send("-1\n")
N = int(nc.recv(1024)) + 1
print "N =", N

# gotta <3 rosetta code
def extended_gcd(aa, bb):
    lastremainder, remainder = abs(aa), abs(bb)
    x, lastx, y, lasty = 0, 1, 1, 0
    while remainder:
        lastremainder, (quotient, remainder) = remainder, divmod(lastremainder, remainder)
        x, lastx = lastx - quotient*x, x
        y, lasty = lasty - quotient*y, y
    return lastremainder, lastx * (-1 if aa < 0 else 1), lasty * (-1 if bb < 0 else 1)

def modinv(a, m):
    g, x, y = extended_gcd(a, m)
    if g != 1:
        raise ValueError
    return x % m

# Calculate t = inverse(request(inverse(C)))
nc.recv(128)    # Prompt
C_inverse = modinv(C, N)
nc.send(str(C_inverse) + "\n")
t_inverse = int(nc.recv(1024))
print "t = " + hex(modinv(t_inverse, N))
=======
"""
# Censorship part 1: Proof of Work - 50 points

I just read the first 12 chars and repeatedly append a suffix and check whether
the hash of the challenge+suffix meets the requirements (last 3 bytes are all
0xff). When a suffix that yields a good hash is found I send it to to the
server and get the part I flag.

This should take 16777216 iterations on average (somewhere around 30-40s on
my machine)

"""

from hashlib import sha1
import socket
import re

nc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
nc.connect(("45.76.89.32", 1234))
nc.settimeout(1)

challenge = nc.recv(12)
print challenge
suffix = 0
while True:
    proof = challenge + str(suffix)
    if sha1(proof).hexdigest().endswith("ffffff"):
        print(proof)
        nc.send(proof + "\n")
        break
    suffix = suffix + 1

# Part 1 flag
print nc.recv(64)

"""
# Censorship part 2: RSA ownage    - 100 points

t - the flag including the appended/preppended random chars
C - the encrypted flag
N, e, d - textbook RSA parameters

C = t^e (mod N)

d is coprime to LCM(p-1, q-1). p-1 and q-1 are even values because p and q
are primes greater than 2. The LCM of two even values must be also even so d
must be odd. Therefore (-1)^d will equal -1, which is congruent to
N-1 modulo N. I can calculate N by requesting (-1)^d and adding 1 to the
response value.

N = 1 + request(-1)                <--- first request

let C' be the modular multiplicative inverse of C modulo N

C' = C^(-1) mod N

then

C'^d = inverse(C)^d = inverse(C^d) = inverse(t)
t = inverse(C')
t = inverse(request(inverse(C)))    <--- second request

After this I used the first web hex to ascii converter I found (too lazy to
make one) to get the flag

"""

# Part 2 first message
prompt = nc.recv(1024)
C = int(re.search("\?\s(\d+)", prompt).group(1))
print "C =", C

# Calculate N = request(-1) + 1
nc.send("-1\n")
N = int(nc.recv(1024)) + 1
print "N =", N

# gotta <3 rosetta code
def extended_gcd(aa, bb):
    lastremainder, remainder = abs(aa), abs(bb)
    x, lastx, y, lasty = 0, 1, 1, 0
    while remainder:
        lastremainder, (quotient, remainder) = remainder, divmod(lastremainder, remainder)
        x, lastx = lastx - quotient*x, x
        y, lasty = lasty - quotient*y, y
    return lastremainder, lastx * (-1 if aa < 0 else 1), lasty * (-1 if bb < 0 else 1)

def modinv(a, m):
    g, x, y = extended_gcd(a, m)
    if g != 1:
        raise ValueError
    return x % m

# Calculate t = inverse(request(inverse(C)))
nc.recv(128)    # Prompt
C_inverse = modinv(C, N)
nc.send(str(C_inverse) + "\n")
t_inverse = int(nc.recv(1024))
print "t = " + hex(modinv(t_inverse, N))
>>>>>>> master
