from pwn import *
from hashlib import sha256
from base58 import b58decode, b58encode

def gcd(a, b):
	while b != 0:
		r = a % b
		a = b
		b = r
	return a

def brent(N):
	if N%2==0:
		return 2
	y,c,m = random.randint(1, N-1),random.randint(1, N-1),random.randint(1, N-1)
	g,r,q = 1,1,1
	while g==1:             
		x = y
		for i in range(r):
			y = ((y*y)%N+c)%N
		k = 0
		while (k<r and g==1):
			ys = y
			for i in range(min(m,r-k)):
				y = ((y*y)%N+c)%N
				q = q*(abs(x-y))%N
			g = gcd(q,N)
			k = k + m
		r = r*2
	if g==N:
		while True:
			ys = ((ys*ys)%N+c)%N
			g = gcd(abs(x-ys),N)
			if g>1:
				break

	return g    

nc = remote("178.62.22.245", 41662)

def POW(nc):
	nc.recv()
	nc.sendline("Y")
	nc.recvuntil("starts with ")
	prefix = nc.recvuntil(":")[:-1]
	partial = prefix
	while len(b58decode(partial)) < 25:
		partial += "a"
	extripmd = b58decode(partial)[:21]
	checksum = sha256(sha256(extripmd).digest()).digest()[:4]
	addr = b58encode(extripmd + checksum)
	nc.sendline(addr)


POW(nc)

# x < y
# x + y < n
# gcd(x, y) * n = x * y => n = lcm(x, y)
#print(nc.recvuntil(":"))
#nc.sendline("1")	# Hope it's a prime /shrug
print(nc.recvuntil("| n = "))
n = int(nc.recvline())
print(n)

factors = {}

while n != 1:
	factor = brent(n)
	print(factor)
	if factor not in factors.keys():
		factors[factor] = 0

	while n % factor == 0:
		factors[factor] += 1
		n //= factor
	print("N = {}".format(n))

prod = 1
for f in factors.keys():
	prod *= factors[f] * 2 + 1

prod = (prod + 1) / 2


nc.sendline(str(prod))

print(nc.recvall())
