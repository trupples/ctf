#!/usr/bin/python2

import sys
from pwn import remote
from base58 import b58encode, b58decode
from hashlib import sha256

host = sys.argv[1]
port = sys.argv[2]

if len(sys.argv) != 3:
	print("Usage: {} <host> <port>\nSolves POW and then passes control to the user".format(sys.argv[0]))
	exit(1)

nc = remote(host, port)

nc.recv()
nc.sendline("Y")
nc.recvuntil("starts with ")
prefix = nc.recvuntil(":")[:-1]
partial = prefix
while len(b58decode(partial)) < 25:
	partial += "a"
partial = b58encode(b58decode(partial)[:25])
eripemd = b58decode(partial)[:21]
checksum = sha256(sha256(eripemd).digest()).digest()[:4]
addr = b58encode(eripemd + checksum)
nc.sendline(addr)

nc.interactive(prompt="")
