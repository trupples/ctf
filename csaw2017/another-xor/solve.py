enchex = "274c10121a0100495b502d551c557f0b0833585d1b27030b5228040d3753490a1c025415051525455118001911534a0052560a14594f0b1e490a010c4514411e070014615a181b02521b580305170002074b0a1a4c414d1f1d171d00151b1d0f480e491e0249010c150050115c505850434203421354424c1150430b5e094d144957080d4444254643"
enc = enchex.decode("hex")[:-32]	# Don't care about the md5

key = ""

# The first 5 chars of the encrypted message should be "flag{"
# Using that the fist 5 chars of the key can be calculated
key += chr(ord(enc[0]) ^ ord('f'))
key += chr(ord(enc[1]) ^ ord('l'))
key += chr(ord(enc[2]) ^ ord('a'))
key += chr(ord(enc[3]) ^ ord('g'))
key += chr(ord(enc[4]) ^ ord('{'))

key = "A qua"

#print(key) # => "A qua" = \x41\x20\x71\x75\x61

def xor(s1,s2):
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))

def repeat(s, l):
    return (s*(int(l/len(s))+1))[:l]

print(xor(enc, repeat(key, len(enc))))
