## Initialisation

### Set video mode
```
0000                 mov     ax, 13h
0003                 int     10h
```

### Set bit 2 of control register 0 - Enables x87 FPU
```
0005                 mov     eax, cr0
0008                 and     ax, 0FFFBh
000B                 or      ax, 2
```

### Set bits 9, 10 of control register 4 - Enables OSFXR, OSXMMEXCPT
```
000E                 mov     cr0, eax
0011                 mov     eax, cr4
0014                 or      ax, 600h
0017                 mov     cr4, eax
```

### Set some variables - TODO figure out wtf these do
```
001A                 mov     word ptr ds:1266h, 0Ah
0020                 mov     bx, 0
```

### Initialise textbox
20 underscores starting at `0x1234`. Input length is stored at `0x7dc8`
```
0023 init_textbox:   mov     byte ptr [bx+1234h], 5Fh ; '_'
0028                 inc     bx
0029                 cmp     bx, 15h
002C                 jle     short init_textbox
002E                 mov     byte ptr ds:7DC8h, 0
```

## Main loop

### Set sleep time 0x10000 micorseconds = 0.065s
```
0033 loop_start:     mov     cx, 1
0036                 xor     dx, dx
```

### Sleep
```
0038 loc_38:         mov     ah, 86h
003A                 int     15h
```

### Draw title
```
003C                 add     byte ptr ds:1278h, 10h ; Change title color
0041                 mov     ax, 1300h              ; Mode 13 - write string
0044 draw_title:     mov     bh, 0                  ; page
0046                 mov     bl, ds:1278h           ; color
004A                 mov     cx, 10h                ; length
004D                 mov     dx, 90Ch               ; row 9 col 12
0050                 mov     bp, 7D60h              ; string address = 0x7c00 (mbr load address) + 0x160 (aEnterFlag)
0053                 int     10h
```

### Draw textbox
```
0055                 mov     ax, 1300h              ; Mode 13
0058 draw_textbox:   mov     bx, 0Fh                ; page 0, white color
005B                 mov     cx, 14h                ; length
005E                 mov     dx, 0C0Ah              ; row 12 col 10
0061                 mov     bp, 1234h              ; string address = 0x1234
0064                 int     10h
```

### Check if textbox isn't full and handle keyboard input
```
0066                 cmp     byte ptr ds:7DC8h, 13h
006B                 jle     keyboard
```

### Verification
#### If textbox is full then check if it begins with "flag"
```
006F                 cmp     dword ptr ds:1234h, 67616C66h
0078                 jnz     wrong
```

#### Load the rest of the flag into xmm0
```
007C                 movaps  xmm0, xmmword ptr ds:1238h
```

#### And the first 16 bytes of the file into xmm5
```
0081                 movaps  xmm5, xmmword ptr ds:7C00h
```

#### Shuffle xmm0 like `AAAABBBBCCCCDDDD` -> `DDDDCCCCAAAABBBB`
Where each letter is a byte and they're in little endian order
```
0086                 pshufd  xmm0, xmm0, 1Eh
```

#### Bitwise and each char with mask starting at `0x198`
```
008B                 mov     si, 8
008E verify:                                 
008E                 movaps  xmm2, xmm0
0091                 andps   xmm2, xmmword ptr [si+7D90h]
```

#### 
0096                 psadbw  xmm5, xmm2
009A                 movaps  xmmword ptr ds:1268h, xmm5
009F                 mov     di, ds:1268h
00A3                 shl     edi, 10h
00A7                 mov     di, ds:1270h
00AB                 mov     dx, si
00AD                 dec     dx
00AE                 add     dx, dx
00B0                 add     dx, dx
00B2                 cmp     edi, [edx+7DA8h]
00BA                 jnz     wrong
00BE                 dec     si
00BF                 test    si, si
00C1                 jnz     short verify
00C3                 mov     byte ptr ds:1278h, 0Ah
00C8                 mov     bx, ds:1266h
00CC                 mov     di, 7D70h
00CF                 test    bx, bx
00D1                 jz      short loc_DF
00D3                 dec     word ptr ds:1266h
00D7                 xor     cx, cx
00D9                 mov     dx, 14h
00DC                 jmp     loc_38
00DF ; ---------------------------------------------------------------------------
00DF
00DF loc_DF:                                 
00DF                 mov     byte ptr ds:7C3Dh, 0
00E4                 mov     word ptr ds:1266h, 0Ah
00EA                 xor     bh, bh
00EC                 mov     bl, ds:7DC9h
00F0                 cmp     bx, 10h
00F3                 jge     loop_start
00F7                 mov     cl, [bx+di]
00F9                 mov     [bx+7D60h], cl
00FD                 inc     byte ptr ds:7DC9h
0101                 mov     dword ptr [bx+7D61h], '>== '
010A                 jmp     loop_start

### Keyboard handling
```
010D keyboard:                               
010D                 mov     ah, 1
010F                 int     16h             ; Check keyboard buffer. Set ZF if empty
0111                 jz      short loop_end
0113                 xor     ah, ah
0115                 int     16h             ; Read char to al
0117                 cmp     al, 8
0119                 jz      short backspace
011B                 cmp     al, 0Dh
011D                 jz      short loop_end
011F                 mov     bx, 1234h
0122                 mov     cl, ds:7DC8h
0126                 add     bx, cx
0128                 mov     [bx], al
012A                 inc     byte ptr ds:7DC8h
012E                 jmp     loop_start
```
0131
0131 backspace:                              
0131                 cmp     byte ptr ds:7DC8h, 1
0136                 jl      loop_start
013A                 mov     ax, 1234h
013D                 dec     byte ptr ds:7DC8h
0141                 mov     bl, ds:7DC8h
0145                 add     bx, ax
0147                 mov     byte ptr [bx], 5Fh ; '_'
014A
014A loop_end:                               
014A                 jmp     loop_start
014D ; ---------------------------------------------------------------------------
014D
014D wrong:                                  
014D                 mov     byte ptr ds:1278h, 4
0152                 mov     di, 7D80h
0155                 jmp     short loc_DF
0155 ; ---------------------------------------------------------------------------
0157                 align 10h
0160 aEnterFlag      db '== ENTER FLAG =='
0170 aCorrect        db '»»» CORRECT! «««'
0180 aWrongFlag      db '!! WRONG FLAG !!'
0190                 db 0FFh
0191                 db 0FFh
0192                 db 0FFh
0193                 db 0FFh
0194                 db 0FFh
0195                 db 0FFh
0196                 db 0FFh
0197                 db 0FFh
0198 mask            db    0
0199                 db 0FFh
019A                 db 0FFh
019B                 db 0FFh
019C                 db 0FFh
019D                 db 0FFh
019E                 db 0FFh
019F                 db 0FFh
01A0                 db    0
01A1                 db 0FFh
01A2                 db 0FFh
01A3                 db 0FFh
01A4                 db 0FFh
01A5                 db 0FFh
01A6                 db 0FFh
01A7                 db 0FFh
01A8                 db  70h ; p
01A9                 db    2
01AA                 db  11h
01AB                 db    2
01AC                 db  55h ; U
01AD                 db    2
01AE                 db  29h ; )
01AF                 db    2
01B0                 db  91h ; æ
01B1                 db    2
01B2                 db  5Eh ; ^
01B3                 db    2
01B4                 db  33h ; 3
01B5                 db    2
01B6                 db 0F9h ; ·
01B7                 db    1
01B8                 db  78h ; x
01B9                 db    2
01BA                 db  7Bh ; {
01BB                 db    2
01BC                 db  21h ; !
01BD                 db    2
01BE                 db    9
01BF                 db    2
01C0                 db  5Dh ; ]
01C1                 db    2
01C2                 db  90h ; É
01C3                 db    2
01C4                 db  8Fh ; Å
01C5                 db    2
01C6                 db 0DFh ; ¯
01C7                 db    2
01C8                 db    0
...
01FD                 db    0
01FE                 db  55h
01FF                 db 0AAh