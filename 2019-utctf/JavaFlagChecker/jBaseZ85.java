import java.io.*;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.util.Arrays;

public final class jBaseZ85
{
  private static final char[] _ALPHA = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#".toCharArray();
  private static final int[] _RALPHA = { 68, 0, 84, 83, 82, 72, 0, 75, 76, 70, 65, 0, 63, 62, 69, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 64, 0, 73, 66, 74, 71, 81, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 77, 0, 78, 67, 0, 0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 79, 0, 80 };
  private static final int _RALSHIFT = 33;
  
  public static String encode(byte[] input)
    throws RuntimeException
  {
    if ((input == null) || (input.length == 0)) {
      throw new IllegalArgumentException("Input is wrong");
    }
    int length = input.length;
    int index = 0;
    byte[] buff = new byte[4];
    
    StringBuilder sb = new StringBuilder(input.length * 5 / 4 + 1);
    while (length >= 4)
    {
      buff[3] = input[(index++)];
      buff[2] = input[(index++)];
      buff[1] = input[(index++)];
      buff[0] = input[(index++)];
      
      sb.append(encodeQuarter(buff));
      
      length -= 4;
    }
    if (length > 0)
    {
      buff = new byte[length];
      for (int i = length - 1; i >= 0; i--) {
        buff[i] = input[(index++)];
      }
      sb.append(encodePadding(buff));
    }
    return sb.toString();
  }
  
  public static byte[] decode(String input)
    throws RuntimeException, UnsupportedEncodingException
  {
    if ((input == null) || (input.length() == 0)) {
      throw new IllegalArgumentException("Input is wrong");
    }
    int length = input.length();
    int index = 0;
    
    char[] buff = new char[5];
    
    ByteBuffer bytebuff = ByteBuffer.allocate(length * 4 / 5);
    while (length >= 5)
    {
      buff[0] = input.charAt(index++);
      buff[1] = input.charAt(index++);
      buff[2] = input.charAt(index++);
      buff[3] = input.charAt(index++);
      buff[4] = input.charAt(index++);
      
      bytebuff.put(decodeQuarter(buff));
      
      length -= 5;
    }
    if (length > 0)
    {
      char[] padding = new char[length];
      for (int i = 0; i < length; i++) {
        padding[i] = input.charAt(index++);
      }
      bytebuff.put(decodePadding(padding));
    }
    bytebuff.flip();
    if (bytebuff.limit() == 0) {
      throw new RuntimeException("Output is empty!");
    }

    try{
    FileOutputStream fos = new FileOutputStream("last-decoded-thing.class");
    fos.write(bytebuff.array(), 0, bytebuff.limit());
    fos.flush();
    fos.close();
  } catch(Exception e) {}
    return Arrays.copyOf(bytebuff.array(), bytebuff.limit());
  }
  
  private static char[] encodeQuarter(byte[] data)
  {
    long value = data[0] & 0xFF | (data[1] & 0xFF) << 8 | (data[2] & 0xFF) << 16 | (data[3] & 0xFF) << 24;
    
    char[] out = new char[5];
    
    out[0] = _ALPHA[((int)(value / 52200625L % 85L))];
    out[1] = _ALPHA[((int)(value / 614125L % 85L))];
    out[2] = _ALPHA[((int)(value / 7225L % 85L))];
    out[3] = _ALPHA[((int)(value / 85L % 85L))];
    out[4] = _ALPHA[((int)(value % 85L))];
    
    return out;
  }
  
  private static char[] encodePadding(byte[] data)
  {
    long value = 0L;
    int length = data.length * 5 / 4 + 1;
    char[] out = new char[length];
    switch (data.length)
    {
    case 3: 
      value |= (data[2] & 0xFF) << 16;
    case 2: 
      value |= (data[1] & 0xFF) << 8;
    }
    value |= data[0] & 0xFF;
    switch (data.length)
    {
    case 3: 
      out[3] = _ALPHA[((int)(value / 614125L % 85L))];
    case 2: 
      out[2] = _ALPHA[((int)(value / 7225L % 85L))];
    }
    out[1] = _ALPHA[((int)(value / 85L % 85L))];
    out[0] = _ALPHA[((int)(value % 85L))];
    
    return out;
  }
  
  private static byte[] decodeQuarter(char[] data)
  {
    long value = 0L;
    
    value += _RALPHA[(data[0] - '!')] * 52200625L;
    value += _RALPHA[(data[1] - '!')] * 614125L;
    value += _RALPHA[(data[2] - '!')] * 7225L;
    value += _RALPHA[(data[3] - '!')] * 85L;
    value += _RALPHA[(data[4] - '!')];
    
    return new byte[] { (byte)(int)(value >>> 24), (byte)(int)(value >>> 16), (byte)(int)(value >>> 8), (byte)(int)value };
  }
  
  private static byte[] decodePadding(char[] data)
  {
    long value = 0L;
    int length = data.length * 4 / 5;
    switch (data.length)
    {
    case 4: 
      value += _RALPHA[(data[3] - '!')] * 614125L;
    case 3: 
      value += _RALPHA[(data[2] - '!')] * 7225L;
    case 2: 
      value += _RALPHA[(data[1] - '!')] * 85L;
    }
    value += _RALPHA[(data[0] - '!')];
    
    byte[] buff = new byte[length];
    for (int i = length - 1; i >= 0; i--) {
      buff[(length - i - 1)] = ((byte)(int)(value >>> 8 * i));
    }
    return buff;
  }
}
