from pwn import *
import math

def qprob2(a, b):
	return abs(a)**2/(abs(a)**2 + abs(b)**2)

r = remote("quantumbomb.live", 1337)

"""
There are 35 suspected bombs; however we can only query in superposition. Can you tell us which are which?
This is bomb 1
Here's the qubit: (-0.828754351676794+0.40937763649913567j) |0> + (0.0906167078342443+0.11393434547803544j) |1>
"""

try:
	for i in range(35):
		print r.recvuntil("Here's the qubit: ", timeout=10)
		s0a = r.recvuntil(")")[1:-1]
		print s0a
		s0 = complex(s0a)
		r.recvuntil("|0> + ")
		s1 = complex(r.recvuntil(")")[1:-1])

		print s0, s1

		epsilon = -0.05

		acc = 0
		while acc < 3.1415/2:
			acc += abs(epsilon)
			print acc

			m00, m01 = math.cos(epsilon), -math.sin(epsilon)
			m10, m11 = math.sin(epsilon), math.cos(epsilon)

			r.recvuntil("decide\n")
			r.sendline("1")
			r.sendline("{},{},{},{}".format(m00, m01, m10, m11))

		r.recvuntil("decide\n")
		r.sendline("2")
		r.recvuntil("Here's the qubit: ")
		l = r.recvline()
		print l
		if "[[1.]] |0>" in l:
			r.sendline("n")
		else:
			r.sendline("y")
except EOFError:
	r.interactive()
