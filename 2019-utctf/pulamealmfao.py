from pwn import *
import math

sh = remote('quantumbomb.live', 1337)

eps = math.pi/180
#eps = 1337


def sendGate():
    global eps
    sh.sendline('1')
    sh.sendline('0+10j,0+10j,0-10j,0-10j' % (math.cos(eps),-math.sin(eps),math.sin(eps),math.cos(eps)))
    sh.recv(4096)
def sendResponse():
    sh.sendline('2')
    rez = sh.recvuntil('Is it a bomb? (y/n)')
    if '[[1.]] |0> + [[0.]] |1>' in rez:
        sh.sendline('y')
    else:
        sh.sendline('n')
    #sh.interactive()

qubit = sh.recvuntil('Options').split("Here's the qubit: ")[1].split('\n')[0]

def parseQubit(qubit):
    first_part = qubit.split(' |0>')[0].strip(')').strip('(')
    fp_j = first_part[2:]
    fp_j = fp_j[fp_j[5:].find('0.')+4:]
    fp_i = first_part.replace(fp_j,'')
    fp_j = fp_j[:-1]
    second_part = qubit.split(' + ')[1].split(' |1>')[0].strip(')').strip('(')
    sp_j = second_part[2:]
    sp_j = sp_j[sp_j[5:].find('0.')+4:]
    sp_i = second_part.replace(sp_j,'')
    sp_j = sp_j[:-1]
    fp_i = float(fp_i)
    fp_j = float(fp_j)
    sp_i = float(sp_i)
    sp_j = float(sp_j)
    return (fp_i, fp_j, sp_i, sp_j)

a,b,c,d = parseQubit(qubit)
print a,b,c,d

try:
    idx = 0
    for j in range(35):
        for i in range(10):
            sendGate()
            idx +=1
            print idx
        sendResponse()
except:
    pass
sh.interactive()
