from pwn import *

for bit in range(7):
	c = chr(1 << bit)
	pt = "AAAAAA{}B".format(c)
	r = process("./encoder")
	r.sendline(pt)
	resp = int(float(r.recvline()))
	print hex((resp<<2) & 0xffffffffffffff)
	r.close()
