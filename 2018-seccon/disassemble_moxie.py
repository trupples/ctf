from pwn import *

def match_binary_pattern(pattern, data):
	values = {}
	for i in range(len(pattern)):
		if pattern[i] == '0' or pattern[i] == '1':
			if pattern[i] != data[i]:
				return None
		else:
			if pattern[i] not in values:
				values[pattern[i]] = 0
			values[pattern[i]] = values[pattern[i]] * 2 + int(data[i])
	values['size'] = len(pattern) / 8
	return values

# Test
assert(repr(match_binary_pattern("110110vvvvvvvvvv", "1101101111000010")) == repr({'v': 0b1111000010, 'size': 2}))
assert(repr(match_binary_pattern("110110vvvvvvvvvv", "1100101111000010")) == repr(None))

def binary(text):
	return text.encode("hex").replace("0", "----").replace("1", "---+").replace("2", "--+-").replace("3", "--++") \
		.replace("4", "-+--").replace("5", "-+-+").replace("6", "-++-").replace("7", "-+++").replace("8", "+---") \
		.replace("9", "+--+").replace("a", "+-+-").replace("b", "+-++").replace("c", "++--").replace("d", "++-+") \
		.replace("e", "+++-").replace("f", "++++").replace("+", "1").replace("-", "0")

# Test
assert(binary("Hello!") == "010010000110010101101100011011000110111100100001")

def location(addr):
	if addr in e.symbols.values():
		return e.symbols.keys()[e.symbols.values().index(addr)]
	return hex(addr)

def handle_instruction_at(start):
	code = binary(e.read(start, 6))
	match = match_binary_pattern("00100110AAAABBBB", code) 
	if match != None:
		disas_data[start] = "and r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00000101AAAABBBB", code) 
	if match != None:
		disas_data[start] = "add r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00101000AAAABBBB", code) 
	if match != None:
		disas_data[start] = "ashl r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00101101AAAABBBB", code) 
	if match != None:
		disas_data[start] = "ashr r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("110000vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "beq {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("110110vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "bge {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("111000vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "bgeu {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("110011vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "bgt {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("110101vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "bgtu {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("110111vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "ble {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("111001vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "bleu {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("110010vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "blt {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("110100vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "bltu {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("110001vvvvvvvvvv", code) 
	if match != None:
		disas_queue.append(start+match['v']*2)
		disas_data[start] = "bne {}".format(location(start+match['v']*2))
		disas_queue.append(start+2)

	match = match_binary_pattern("00110101xxxxxxxx", code) 
	if match != None:
		disas_data[start] = "brk"
		disas_queue.append(start+2)

	match = match_binary_pattern("00001110AAAABBBB", code) 
	if match != None:
		disas_data[start] = "cmp r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("1001AAAAiiiiiiii", code) 
	if match != None:
		disas_data[start] = "dec r{}, {}".format(match['A'], match['i'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00110001AAAABBBB", code) 
	if match != None:
		disas_data[start] = "div r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("1010AAAASSSSSSSS", code) 
	if match != None:
		disas_data[start] = "gsr r{}, special{}".format(match['A'], match['S'])
		disas_queue.append(start+2)

	match = match_binary_pattern("1000AAAAiiiiiiii", code) 
	if match != None:
		disas_data[start] = "inc r{}, {}".format(match['A'], match['i'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00100101AAAAxxxx", code) 
	if match != None:
		disas_data[start] = "jmp r{}".format(location(match['A']))
		disas_queue.append(start+2)

	match = match_binary_pattern("00011010xxxxxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_queue.append(match['i'])
		disas_data[start] = "jmpa {}".format(location(match['i']))
		disas_queue.append(start+6)

	match = match_binary_pattern("00011001AAAAxxxx", code) 
	if match != None:
		disas_data[start] = "jsr r{}".format(match['A'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00000011xxxxxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_queue.append(match['i'])
		disas_data[start] = "jsra {}".format(location(match['i']))
		disas_queue.append(start+6)

	match = match_binary_pattern("00011100AAAABBBB", code) 
	if match != None:
		disas_data[start] = "ld.b r{}, [r{}]".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00001010AAAABBBB", code) 
	if match != None:
		disas_data[start] = "ld.l r{}, [r{}]".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00100001AAAABBBB", code) 
	if match != None:
		disas_data[start] = "ld.s r{}, [r{}]".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00011101AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "lda.b r{}, [{}]".format(match['A'], match['i'])
		disas_queue.append(start+6)

	match = match_binary_pattern("00001000AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "lda.l r{}, [{}]".format(match['A'], match['i'])
		disas_queue.append(start+6)

	match = match_binary_pattern("00100010AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "lda.s r{}, [{}]".format(match['A'], match['i'])
		disas_queue.append(start+6)

	match = match_binary_pattern("00000001AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "ldi.l r{}, {}".format(match['A'], match['i'])
		disas_queue.append(start+6)

	match = match_binary_pattern("00011011AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "ldi.b r{}, {}".format(match['A'], match['i'])
		disas_queue.append(start+6)

	match = match_binary_pattern("00100000AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "ldi.s r{}, {}".format(match['A'], match['i'])
		disas_queue.append(start+6)

	match = match_binary_pattern("00110110AAAABBBBiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "ldo.b r{}, [r{} + {}]".format(match['A'], match['B'], match['i'])
		disas_queue.append(start+4)

	match = match_binary_pattern("00001100AAAABBBBiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "ldo.l r{}, [r{} + {}]".format(match['A'], match['B'], match['i'])
		disas_queue.append(start+4)

	match = match_binary_pattern("00111000AAAABBBBiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "ldo.s r{}, [r{} + {}]".format(match['A'], match['B'], match['i'])
		disas_queue.append(start+4)

	match = match_binary_pattern("00100111AAAABBBB", code) 
	if match != None:
		disas_data[start] = "lshr r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00110011AAAABBBB", code) 
	if match != None:
		disas_data[start] = "mod r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00000010AAAABBBB", code) 
	if match != None:
		disas_data[start] = "mov r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00101111AAAABBBB", code) 
	if match != None:
		disas_data[start] = "mul r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00010101AAAABBBB", code) 
	if match != None:
		disas_data[start] = "mul.x r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00101010AAAABBBB", code) 
	if match != None:
		disas_data[start] = "neg r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00001111xxxxxxxx", code) 
	if match != None:
		disas_data[start] = "nop"
		disas_queue.append(start+2)

	match = match_binary_pattern("00101100AAAABBBB", code) 
	if match != None:
		disas_data[start] = "not r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00101011AAAABBBB", code) 
	if match != None:
		disas_data[start] = "or r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00000111AAAABBBB", code) 
	if match != None:
		disas_data[start] = "pop r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00000110AAAABBBB", code) 
	if match != None:
		disas_data[start] = "push r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00000100xxxxxxxx", code) 
	if match != None:
		disas_data[start] = "ret"
		disas_queue.append(start+2)

	match = match_binary_pattern("00010000AAAABBBB", code) 
	if match != None:
		disas_data[start] = "sex.b r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00010001AAAABBBB", code) 
	if match != None:
		disas_data[start] = "sex.s r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("1011AAAASSSSSSSS", code) 
	if match != None:
		disas_data[start] = "ssr special{}, a{}".format(match['S'], match['A'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00011110AAAABBBB", code) 
	if match != None:
		disas_data[start] = "st.b [r{}], r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00001011AAAABBBB", code) 
	if match != None:
		disas_data[start] = "st.l [r{}], r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00100011AAAABBBB", code) 
	if match != None:
		disas_data[start] = "st.s [r{}], r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00011111AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "sta.b"
		disas_queue.append(start+6)

	match = match_binary_pattern("00001001AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "sta.l"
		disas_queue.append(start+6)

	match = match_binary_pattern("00100100AAAAxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "sta.s"
		disas_queue.append(start+6)

	match = match_binary_pattern("00110111AAAABBBBiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "sto.b"
		disas_queue.append(start+4)

	match = match_binary_pattern("00001101AAAABBBBiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "sto.l"
		disas_queue.append(start+4)

	match = match_binary_pattern("00111001AAAABBBBiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "sto.s"
		disas_queue.append(start+4)

	match = match_binary_pattern("00101001AAAABBBB", code) 
	if match != None:
		disas_data[start] = "sub r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00110000xxxxxxxxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", code) 
	if match != None:
		disas_data[start] = "swi"
		disas_queue.append(start+6)

	match = match_binary_pattern("00110010AAAABBBB", code) 
	if match != None:
		disas_data[start] = "udiv r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00110100AAAABBBB", code) 
	if match != None:
		disas_data[start] = "umod r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00010100AAAABBBB", code) 
	if match != None:
		disas_data[start] = "umul.x r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00101110AAAABBBB", code) 
	if match != None:
		disas_data[start] = "xor r{}, r{}".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00010010AAAABBBB", code) 
	if match != None:
		disas_data[start] = "zex.b r{}, LOW8(r{})".format(match['A'], match['B'])
		disas_queue.append(start+2)

	match = match_binary_pattern("00010011AAAABBBB", code) 
	if match != None:
		disas_data[start] = "zex.s r{}, LOW16(r{})".format(match['A'], match['B'])
		disas_queue.append(start+2)


e = ELF(sys.argv[1])

disas_queue = []
disas_data = {}

disas_queue.append(e.symbols["_start"])
disas_queue.append(5248)

while len(disas_queue) > 0:
	pos = disas_queue[0]
	disas_queue = disas_queue[1:]
	if pos not in disas_data:
		handle_instruction_at(pos)

def print_disassembly(sym):
	if sym in e.symbols:
		pos = e.symbols[sym]
		symname = sym
	else:
		if sym in e.symbols.values():
			pos = sym
			symname = e.symbols.keys()[e.symbols.values().index(sym)]
		else:
			pos = sym
			symname = hex(sym)

	print "{}:".format(symname)
	while pos < e.symbols["_end"]:
		if pos in disas_data:
			print hex(pos).rjust(5) + " " + disas_data[pos]
			if disas_data[pos] == "ret":
				break
		pos += 1
		if pos in e.symbols.values():
			break


print "main:"
pos = e.symbols["main"]
r3 = None
while pos < e.symbols["_end"]:
	if pos in disas_data:
		print hex(pos).rjust(5) + " " + disas_data[pos]
		if disas_data[pos] == "ret":
			break
		if disas_data[pos].startswith("ldi.l r3, "):
			r3 = int(disas_data[pos][10:])
		if disas_data[pos] == "jsr r8":
			if r3 != None:
				print "; => {}".format(repr(e.string(r3)))
			r3 = None
	pos += 1
	if pos in e.symbols.values():
		break

print_disassembly("decode")
print_disassembly(0x1d72)