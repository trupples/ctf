from pwn import *
import string

ct = open("./youfool!.exe", "rb").read()

pt = ""
knownkey = [None]*60

def match(start, plain):
	for i in range(start, start+len(plain)):
		if knownkey[i%60] == None:
			k = xor(plain[i-start], ct[i])
			assert(k in string.ascii_letters + string.punctuation + string.digits)
			knownkey[i%60] = k
		else:
			assert(knownkey[i%60] == xor(plain[i-start], ct[i]))

match(0, "%PDF")

lastlen = 0
for i in range(len(ct)):
	if lastlen % 60 == 0 and i > 0:
		assert(len(pt) == 60)
		print("{}: {}".format(lastlen-60, bytes(pt)))
		pt = ""
	if knownkey[i%60] != None:
		pt += xor(ct[i], knownkey[i%60])
	else:
		pt += "?"
	lastlen += 1

#open("rahat.pdf", "w").write(pt)

