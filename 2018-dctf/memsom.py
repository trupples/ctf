import md5, base64

def rot13onlylowercase(s):
	r = ""
	for c in s:
		c = ord(c)
		if c >= ord('a') and c <= ord('z'):
			r += chr((c-ord('a')+13)%26+ord('a'))
		else:
			r += chr(c)
	return r

def md5step(s):
	a = md5.new()
	a.update(s)
	return a.hexdigest()

for c in range(256):
	c = rot13onlylowercase(chr(c))
	print c, ord(c), md5step(md5step(base64.b64encode(c)))