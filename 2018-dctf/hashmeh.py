hashbytes = [0x26, 0xB7, 0xF8, 0xB7, 0xDD, 0x2E, 0x2E, 0xBF, 0xD4, 0x17,
0x85, 0xC6, 0xE0, 0xB0, 0x32, 0x9D, 0xD5, 0x39, 0x93, 0x3C,
0x35, 0x40, 0xFB, 0xB0, 0xF6, 0x32, 0x71, 0xC2, 0xEA, 0x6A,
0xE1, 0xA0, 0xE1, 0x66, 0x8C, 0x57, 0x46, 0x18, 0x2F, 0xBE,
3, 6, 0x51, 9, 0xFF, 0xC7, 0x62, 0xDD, 0xE8, 0x96, 0xB1,
0x4A, 0x1A, 0xC6, 0xD1, 0xB1, 0x9D, 0x60, 0xF9, 0x71, 0xF1,
0x91, 0x98, 0xD6]

hashstring = "".join([hex(x+256)[-2:] for x in hashbytes])

print hashstring

hashstring = ""

for i in range(0, len(hashbytes), 8):
	hashstring = "".join([hex(x+256)[-2:] for x in hashbytes[i:i+8]]) + hashstring

print hashstring

hashstring = ""

for i in range(0, len(hashbytes), 8):
	hashstring += "".join([hex(x+256)[-2:] for x in hashbytes[i:i+8][::-1]])

print hashstring