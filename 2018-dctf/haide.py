from pwn import *
import threading

context.log_level = 'error'

def force(portstart, portend):
	for port in range(portstart, portend):
		try:
			r = remote("104.248.38.191", port, timeout=1, typ="tcp")
			print r.recvall()

			r = remote("104.248.38.191", port, timeout=1, typ="udp")
			print r.recvall()
		except:
			pass


for port in range(0, 65536, 1000):
	process=threading.Thread(target=force,  args=[port, port+1000])
	process.start()